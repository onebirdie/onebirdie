package haizhi.log.product.parser;

import haizhi.log.product.params.CSVParams;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CSVParser extends BaseParser<CSVParams>{

    public CSVParser(String paramStr) {
        super(paramStr);

        params = new CSVParams(paramStr);
    }

    public CSVParser(JSONObject jsonObject) {
        super(jsonObject);

        params = new CSVParams(jsonObject);
    }

    @Override
    public HashMap<String, Object> onParse(String text) {
        ArrayList<String> fields = params.getCsvFields();
        String sep = params.getFieldSeparator();

        List<String> items = Arrays.asList(text.split("(?<!\\\\)" + sep));

        if (items.size() != fields.size()) {
            return null;
        }

        HashMap<String, Object> result = new HashMap<>();

        for (int i = 0; i < items.size(); i++) {
            result.put(fields.get(i).trim(), items.get(i));
        }

        return result;
    }
}
