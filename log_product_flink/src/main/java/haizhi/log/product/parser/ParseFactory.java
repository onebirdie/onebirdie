package haizhi.log.product.parser;

import haizhi.log.product.params.BaseParams;
import org.json.simple.JSONObject;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

public class ParseFactory {
    private static final String DEFAULT_PARSE_TYPE = "grok";
    private static final String XML_PARSE_TYPE = "xml";
    private static final String CSV_PARSE_TYPE = "csv";
    private static final String KV_PARSE_TYPE = "kv";
    private static final String JSON_PARSE_TYPE = "json";

    private static final HashMap<String, Class<? extends BaseParser>> parseTypeMap =
            new HashMap<String, Class<? extends BaseParser>>(){
        {
            put(DEFAULT_PARSE_TYPE, GrokParser.class);
            put(XML_PARSE_TYPE, XmlParser.class);
            put(CSV_PARSE_TYPE, CSVParser.class);
            put(KV_PARSE_TYPE, KVParser.class);
            put(JSON_PARSE_TYPE, JsonParser.class);
        }
    };

    public static BaseParser<? extends BaseParams> getParser(String parseType, String rule) {
        if (! parseType.contains(parseType)){
            parseType = DEFAULT_PARSE_TYPE;
        }

        Class<? extends BaseParser> clazz = parseTypeMap.get(parseType);

        Constructor<? extends BaseParser> constructor = null;
        try {
            constructor = clazz.getConstructor(String.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        try {
            return constructor.newInstance(rule);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static BaseParser<? extends BaseParams> getParser(String parseType, JSONObject rule) {
        if (! parseType.contains(parseType)){
            parseType = DEFAULT_PARSE_TYPE;
        }

        Class<? extends BaseParser> clazz = parseTypeMap.get(parseType);

        Constructor<? extends BaseParser> constructor = null;
        try {
            constructor = clazz.getConstructor(JSONObject.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        try {
            return constructor.newInstance(rule);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static HashMap<String, Object> getParsedResult(BaseParser<? extends BaseParams> parser, String text) {
        try {
            return parser.parse(text);
        } catch (Exception e) {
            e.printStackTrace();

            return null;
        }
    }
}
