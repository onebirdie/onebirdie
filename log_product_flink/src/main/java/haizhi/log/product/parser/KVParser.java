package haizhi.log.product.parser;

import com.google.common.base.Splitter;
import haizhi.log.product.params.KVParams;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class KVParser extends BaseParser<KVParams>{
    public KVParser(JSONObject jsonObject) {
        super(jsonObject);

        params = new KVParams(jsonObject);
    }

    public KVParser(String paramStr) {
        super(paramStr);

        params = new KVParams(paramStr);
    }

    @Override
    public HashMap<String, Object> onParse(String text) {
        HashMap<String, Object> result = new HashMap<>();

        Map<String, String> map;
        try {
            map = Splitter
                    .onPattern(params.getFieldSeparator())
                    .trimResults()
                    .omitEmptyStrings()
                    .withKeyValueSeparator(
                            Splitter.onPattern(params.getKvSeparator())
                    )
                    .split(text);
        } catch (Exception e) {
            e.printStackTrace();
            return result;
        }

        boolean needStripQuoted = params.needStripQuote();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            String[] keys = entry.getKey().split("\\s+");
            String key = keys[keys.length - 1];
            String value = entry.getValue().trim();

            if (needStripQuoted) {
                key = ParseUtils.stripQuote(key);
                value = ParseUtils.stripQuote(value);
            }

            result.put(key, value);
        }
        return result;
    }
}
