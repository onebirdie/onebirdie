package haizhi.log.product.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParseUtils {
    public static String stripPrefix(String regex, String text){
        if (regex == null || regex.isEmpty())
            return text;

        if (! regex.startsWith("^"))
            regex = "^" + regex;

        return text.replaceFirst(regex,  "");
    }

    public static String stripSuffix(String regex, String text){
        if (regex == null || regex.isEmpty())
            return text;

        if (! regex.endsWith("$"))
            regex = regex + "$";

        return text.replaceAll(regex, "");
    }

    public static String stripQuote(String text) {
        if (text.startsWith("'") || text.startsWith("\""))
            text = text.substring(1);

        if (text.endsWith("'") || text.endsWith("\""))
            text = text.substring(0, text.length() - 1);

        return text;
    }

    public static String toWordString(String text) {
        return text.replaceAll("[^a-zA-Z0-9_]", "_");
    }

    public static String toAlphaNumString(String text) {
        return text.replaceAll("[^a-zA-Z0-9]", "");
    }

    public static ArrayList<String> toWordString(ArrayList<String> texts) {
        ArrayList<String> result = new ArrayList<>();

        for (String text : texts) {
            result.add(toWordString(text));
        }

        return result;
    }

    public static ArrayList<String> toAlphaNumString(ArrayList<String> texts) {
        ArrayList<String> result = new ArrayList<>();

        for (String text : texts) {
            result.add(toAlphaNumString(text));
        }

        return result;
    }

    public static String pythonRegexToJava(String regex) {
        // 将 (?P<>) 转换为 (?<>)
        return regex.replaceAll("\\(\\?P<", "(?<");
    }

    public static ArrayList<String> extractFieldNames(String regex){
        String fieldReg = "(?<=\\(\\?<)\\w+";
//        Pattern pattern = Pattern.compile(fieldReg);
        Pattern pattern = RegexPool.getRegexPattern(fieldReg);
        Matcher matcher = pattern.matcher(regex);

        ArrayList<String> fields = new ArrayList<>();
        while (matcher.find()) {
            fields.add(matcher.group());
        }

        return fields;
    }

    public static String toAlphaNumRegex(String regex, HashMap<String, String> fieldNameMap){
        ArrayList<String> fields = extractFieldNames(regex);

        for (String field : fields) {
            String newField = toAlphaNumString(field);
            fieldNameMap.put(newField, field);

            regex = regex.replaceAll("\\(\\?<" + field + ">", "(?<" + newField + ">");
        }

        return regex;
    }

    public static String toWordRegex(String regex, HashMap<String, String> fieldNameMap){
        ArrayList<String> fields = extractFieldNames(regex);

        for (String field : fields) {
            String newField = toWordString(field);
            fieldNameMap.put(newField, field);

            regex = regex.replaceAll("\\(\\?<" + field + ">", "(?<" + newField + ">");
        }

        return regex;
    }

    public static HashMap<String, Object> extractFieldResult(String regex, String text){
//        HashMap<String, String> fieldNameMap =  new HashMap<>();

        // regex = toWordRegex(regex, fieldNames);
        // java只支持字母数字和下划线
//        regex = toAlphaNumRegex(regex, fieldNameMap);
        Object[] tuple = RegexPool.getValidateJavaRegex(regex);
        regex = (String)tuple[0];
        HashMap<String, String> fieldNameMap = (HashMap<String, String>)tuple[1];

        HashMap<String, Object> result = new HashMap<>();

//        Pattern pattern = Pattern.compile(regex);
        Pattern pattern = RegexPool.getRegexPattern(regex);
        Matcher matcher = pattern.matcher(text);

        while (matcher.find()) {
            for (Map.Entry<String, String> stringStringEntry : fieldNameMap.entrySet()) {
                String value = matcher.group(stringStringEntry.getKey());
                if (value == null || value.isEmpty()) {
                    continue;
                }

                result.put(stringStringEntry.getValue(), value);
            }
        }

        return result;
    }

    public static String selectLineRange(String line, int startFrom, int endFrom){
        if (startFrom == 1 && endFrom == 1)
            return line;

        String[] lines = line.split("\n");

        int start = startFrom - 1;
        int end = lines.length - endFrom + 1;

        if (start > end) {
            return null;
        }

        String[] newLines = Arrays.copyOfRange(lines, start, end);

        return String.join("\n", newLines);
    }
}
