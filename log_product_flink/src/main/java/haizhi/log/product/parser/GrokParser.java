package haizhi.log.product.parser;

import haizhi.log.product.params.GrokParams;
import org.json.simple.JSONObject;

import java.util.HashMap;

public class GrokParser extends BaseParser<GrokParams> {

    public GrokParser(JSONObject jsonObject) {
        super(jsonObject);

        params = new GrokParams(jsonObject);
    }

    public GrokParser(String paramStr) {
        super(paramStr);

        params = new GrokParams(paramStr);
    }

    @Override
    public HashMap<String, Object> onParse(String text) {
        String regex = params.getRegex();
//        regex = ParseUtils.pythonRegexToJava(regex);
        regex = RegexPool.getPythonToJavaRegex(regex);

        return ParseUtils.extractFieldResult(regex, text);
    }
}
