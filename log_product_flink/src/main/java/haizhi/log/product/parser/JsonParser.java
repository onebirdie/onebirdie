package haizhi.log.product.parser;

import com.google.gson.JsonElement;
import haizhi.log.product.params.JsonParams;
import haizhi.log.product.transform.JsonTransform;
import haizhi.log.product.transform.TransformException;
import haizhi.log.product.traverse.JsonTraverser;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class JsonParser extends BaseParser<JsonParams>{
    public JsonParser(JSONObject jsonObject) {
        super(jsonObject);

        params = new JsonParams(jsonObject);
    }

    public JsonParser(String paramStr) {
        super(paramStr);

        params = new JsonParams(paramStr);
    }

    @Override
    public HashMap<String, Object> onParse(String text) {
        try {
            JsonElement jsonElement = new JsonTransform().transform(text);
            if (jsonElement.isJsonArray() || jsonElement.isJsonNull())
                return new HashMap<>(0);

            HashMap<String, Object> result = new HashMap<>();
            for (Map.Entry<String, JsonElement> stringJsonElementEntry : jsonElement.getAsJsonObject().entrySet()) {
                result.putAll(new JsonTraverser(stringJsonElementEntry, params.isNested()).traverse());
            }

            return result;
        } catch (TransformException e) {
            e.printStackTrace();

            return new HashMap<>(0);
        }
    }
}
