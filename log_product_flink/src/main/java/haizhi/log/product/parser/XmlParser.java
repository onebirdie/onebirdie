package haizhi.log.product.parser;

import haizhi.log.product.params.XmlParams;
import haizhi.log.product.transform.TransformException;
import haizhi.log.product.transform.XmlTransform;
import haizhi.log.product.traverse.XmlTraverser;
import org.json.simple.JSONObject;

import java.util.HashMap;

public class XmlParser extends BaseParser<XmlParams>{
    public XmlParser(JSONObject jsonObject) {
        super(jsonObject);

        params = new XmlParams(jsonObject);
    }

    public XmlParser(String paramStr) {
        super(paramStr);

        params = new XmlParams(paramStr);
    }

    @Override
    public HashMap<String, Object> onParse(String text) {
        try {
            return new XmlTraverser(new XmlTransform().transform(text), params.isNested())
                    .traverse();
        } catch (TransformException e) {
            e.printStackTrace();

            return new HashMap<>(0);
        }
    }
}
