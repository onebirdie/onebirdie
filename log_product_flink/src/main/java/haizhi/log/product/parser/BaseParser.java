package haizhi.log.product.parser;

import haizhi.log.product.params.BaseParams;
import haizhi.log.product.transform.TransformUtils;
import org.json.simple.JSONObject;

import java.util.HashMap;

public abstract class BaseParser<T extends BaseParams> {
    protected T params;

    public BaseParser(String paramStr) {
    }

    public BaseParser(JSONObject jsonObject) {
    }

    public HashMap<String, Object> parse(String text) {
        text = ParseUtils.selectLineRange(text, params.getBeginLine(), params.getEndLine());
        if (text == null)
            return new HashMap<>(0);

        text = ParseUtils.stripPrefix(params.getStripPrefix(), text);
        text = ParseUtils.stripSuffix(params.getStripSuffix(), text);

//        if (params.needStripQuote()) {
//            text = ParseUtils.stripQuote(text);
//        }

        HashMap<String, Object> result = onParse(text);

        JSONObject transformMap = params.getTransform();
        return TransformUtils.transform(result, transformMap);
    }

    protected abstract HashMap<String, Object> onParse(String text);
}
