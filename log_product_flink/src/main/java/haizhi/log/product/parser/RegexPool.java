package haizhi.log.product.parser;

import java.util.HashMap;
import java.util.regex.Pattern;

public class RegexPool {
    private static HashMap<String, Pattern> patternHashMap = new HashMap<>();
    private static HashMap<String, String> pythonToJavaRegexHashMap = new HashMap<>();
    private static HashMap<String, Object[]> validateJavaRegexHashMap = new HashMap<>();

    public static Pattern getRegexPattern(String regex) {
        if (!patternHashMap.containsKey(regex)) {
            patternHashMap.put(regex, Pattern.compile(regex));
        }

        return patternHashMap.get(regex);
    }

    public static String getPythonToJavaRegex(String regex) {
        if (!pythonToJavaRegexHashMap.containsKey(regex)) {
            pythonToJavaRegexHashMap.put(regex, ParseUtils.pythonRegexToJava(regex));
        }

        return pythonToJavaRegexHashMap.get(regex);
    }

    public static Object[] getValidateJavaRegex(String regex) {
        if (!validateJavaRegexHashMap.containsKey(regex)) {
            HashMap<String, String> fieldNameMap =  new HashMap<>();
            String validRegex = ParseUtils.toAlphaNumRegex(regex, fieldNameMap);

            validateJavaRegexHashMap.put(regex, new Object[] {validRegex, fieldNameMap});
        }

        return validateJavaRegexHashMap.get(regex);
    }
}
