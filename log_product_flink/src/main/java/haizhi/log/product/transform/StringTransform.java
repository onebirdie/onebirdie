package haizhi.log.product.transform;

public class StringTransform implements ITransform<Object, String> {
    @Override
    public String transform(Object input) throws TransformException {
        try {
            return String.valueOf(input);
        } catch (ClassCastException e) {
            throw new TransformException(e.getMessage());
        }
    }
}