package haizhi.log.product.transform;

import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TransformUtils {
    private static final HashMap<String, ITransform> transformHashMap = new HashMap<String, ITransform>() {
        {
            try {
                put("string", StringTransform.class.newInstance());
                put("int", IntegerTransform.class.newInstance());
                put("long", LongTransform.class.newInstance());
                put("float", FloatTransform.class.newInstance());
                put("double", DoubleTransform.class.newInstance());
                put("xml", XmlTransform.class.newInstance());
                put("json", JsonTransform.class.newInstance());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    };

    private static ITransform getTransformer(String type) {
        if (! transformHashMap.containsKey(type))
            type = "string";

        return transformHashMap.get(type);
    }

    public static Object transformValue(Object value, String type) {
        try {
            return getTransformer(type).transform(value);
        } catch (TransformException e) {
            e.printStackTrace();

            return value;
        }
    }

    private static Object getTypedValue(HashMap<String, Object> valueMap, String key, String type) {
        if (valueMap.containsKey(key)) {
            Object value = valueMap.get(key);

            if (value != null) {
                return transformValue(value, type);
            } else {
                return valueMap.get(key);
            }
        } else {
            return null;
        }
    }

    private static HashMap<String, Object> transform(HashMap<String, Object> valueMap, HashMap<String, String> typeMap) {
        for (Map.Entry<String, String> stringStringEntry : typeMap.entrySet()) {
            String key = stringStringEntry.getKey();
            String type = stringStringEntry.getValue();

            Object value = getTypedValue(valueMap, key, type);
            if(value != null) {
                valueMap.put(key, value);
            }
        }

        return valueMap;
    }


    public static HashMap<String, Object> transform(HashMap<String, Object> valueMap, JSONObject typeMap) {
        for (Object o : typeMap.keySet()) {
            String key = (String)o;
            String type = (String)typeMap.get(o);

            Object value = getTypedValue(valueMap, key, type);
            if(value != null) {
                valueMap.put(key, value);
            }
        }

        return valueMap;
    }
}
