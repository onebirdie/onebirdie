package haizhi.log.product.transform;

public class FloatTransform implements ITransform<Object, Float> {
    @Override
    public Float transform(Object input) throws TransformException {
        try {
            return Float.parseFloat(String.valueOf(input));
        } catch (ClassCastException e) {
            throw new TransformException(e.getMessage());
        } catch (NumberFormatException e) {
            throw new TransformException(e.getMessage());
        }
    }
}