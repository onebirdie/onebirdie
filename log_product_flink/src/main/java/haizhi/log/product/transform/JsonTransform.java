package haizhi.log.product.transform;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

public class JsonTransform implements ITransform<Object, JsonElement> {
    @Override
    public JsonElement transform(Object input) throws TransformException {
        JsonElement jsonElement = null;
        try {
             jsonElement = new JsonParser().parse(String.valueOf(input));
        } catch (ClassCastException e) {
            throw new TransformException(e.getMessage());
        } catch (JsonSyntaxException e) {
            throw new TransformException(e.getMessage());
        }


        if (jsonElement.isJsonObject())
            return jsonElement.getAsJsonObject();
        else if (jsonElement.isJsonArray())
            throw new TransformException("json array is not supported at the moment");
        else
            throw new TransformException("not a json object or json array");
    }
}