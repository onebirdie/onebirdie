package haizhi.log.product.transform;

import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

public class XmlTransform implements ITransform<Object, Element> {
    @Override
    public Element transform(Object input) throws TransformException{
        try {
            return DocumentHelper.parseText(String.valueOf(input)).getRootElement();
        } catch (ClassCastException e) {
            throw new TransformException(e.getMessage());
        } catch (DocumentException e) {
            throw new TransformException(e.getMessage());
        }
    }
}