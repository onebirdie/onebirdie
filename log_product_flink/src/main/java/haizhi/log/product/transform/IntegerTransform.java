package haizhi.log.product.transform;

public class IntegerTransform implements ITransform<Object, Integer> {
    @Override
    public Integer transform(Object input) throws TransformException {
        try {
            return (int)(Float.parseFloat(String.valueOf(input)));
        } catch (ClassCastException e) {
            throw new TransformException(e.getMessage());
        } catch (NumberFormatException e) {
            throw new TransformException(e.getMessage());
        }
    }
}
