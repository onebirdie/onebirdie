package haizhi.log.product.transform;

public class LongTransform implements ITransform<Object, Long> {
    @Override
    public Long transform(Object input) throws TransformException {
        try {
            return (long)(Double.parseDouble(String.valueOf(input)));
        } catch (ClassCastException e) {
            throw new TransformException(e.getMessage());
        } catch (NumberFormatException e) {
            throw new TransformException(e.getMessage());
        }
    }
}