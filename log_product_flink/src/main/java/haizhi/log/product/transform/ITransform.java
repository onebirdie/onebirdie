package haizhi.log.product.transform;

public interface ITransform<T, R>  {
    R transform(T input) throws TransformException;
}
