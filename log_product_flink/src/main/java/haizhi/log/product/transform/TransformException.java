package haizhi.log.product.transform;

public class TransformException extends Exception {

    public TransformException() {
    }

    public TransformException(String message) {
        super(message);
    }
}
