package haizhi.log.product.transform;

public class DoubleTransform implements ITransform<Object, Double> {
    @Override
    public Double transform(Object input) throws TransformException {
        try {
            return Double.parseDouble(String.valueOf(input));
        } catch (ClassCastException e) {
            throw new TransformException(e.getMessage());
        } catch (NumberFormatException e) {
            throw new TransformException(e.getMessage());
        }
    }
}
