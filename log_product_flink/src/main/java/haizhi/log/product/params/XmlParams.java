package haizhi.log.product.params;

import org.json.simple.JSONObject;

public class XmlParams extends BaseParams {
    public XmlParams(JSONObject jsonObject) {
        super(jsonObject);
    }

    public XmlParams(String jsonStr) {
        super(jsonStr);
    }

    @Override
    protected String getBeginLineKey() {
        if (containsKey(KEY_BEGIN_LINE)) {
            return KEY_BEGIN_LINE;
        }

        return "xml_begin_line";
    }

    @Override
    protected String getEndLineKey() {
        if (containsKey(KEY_END_LINE)) {
            return KEY_END_LINE;
        }

        return "xml_end_line";
    }

    @Override
    protected String getNestedKey() {
        if (containsKey(KEY_NESTED)) {
            return KEY_NESTED;
        }

        return "xml_nested";
    }
}
