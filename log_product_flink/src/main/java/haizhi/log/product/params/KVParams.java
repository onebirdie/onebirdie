package haizhi.log.product.params;

import org.json.simple.JSONObject;

public class KVParams extends BaseParams {
    public static final String KEY_KV_SEPARATOR = "kv_separator";

    public KVParams(JSONObject jsonObject) {
        super(jsonObject);
    }

    public KVParams(String jsonStr) {
        super(jsonStr);
    }

    @Override
    protected String getFieldSepKey() {
        return "field_separator";
    }

    @Override
    protected String getDefaultSep() {
        return "\\s+";
    }

    public String getKvSeparator() {
        return getValidStringParam(KEY_KV_SEPARATOR, "\\s+");
    }
}
