package haizhi.log.product.params;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.ArrayList;

public class BaseParams {
    public static final String KEY_BEGIN_LINE = "begin_line";
    public static final String KEY_END_LINE = "end_line";
    public static final String KEY_STRIP_PREFIX = "no_prefix_rule";
    public static final String KEY_STRIP_SUFFIX = "no_suffix_rule";
    public static final String KEY_NESTED = "nested";
    public static final String KEY_FIELD_SEP = "field_sep";
    public static final String KEY_STRIP_QUOTE = "ignore_quote";
    public static final String KEY_TRANSFORM = "transform";

    private JSONObject params;

    public BaseParams(JSONObject jsonObject) {
        this.params = jsonObject;
    }

    public BaseParams(String jsonStr) {
        try {
            this.params = (JSONObject)(new JSONParser().parse(jsonStr));
        } catch (ParseException e) {
            e.printStackTrace();

            this.params = new JSONObject();
        }
    }

    public boolean isParamInValid(String key) {
        return ! params.containsKey(key) || params.get(key) == null;
    }

    public boolean containsKey(String key) {
        return params.containsKey(key);
    }

    protected String getBeginLineKey(){
        return KEY_BEGIN_LINE;
    }

    protected String getEndLineKey(){
        return KEY_END_LINE;
    }

    protected String getNestedKey(){
        return KEY_NESTED;
    }

    protected String getFieldSepKey(){
        return KEY_FIELD_SEP;
    }

    protected String getDefaultSep(){return ",";}

    public String getStringParam(String key, String value){
        if (isParamInValid(key))
            return value;

        return (String)params.get(key);
    }

    public String getValidStringParam(String key, String value){
        if (isParamInValid(key))
            return value;

        String newValue = getStringParam(key, value);
        return newValue.isEmpty() ? value : newValue;
    }

    public Integer getIntegerParam(String key, Integer value){
        if (isParamInValid(key))
            return value;

        return Integer.valueOf(params.get(key).toString());
    }

    public Boolean getBooleanParam(String key, Boolean value){
        if (isParamInValid(key))
            return value;

        return Boolean.valueOf(params.get(key).toString());
    }

    public ArrayList<String> getStringListParam(String key, ArrayList<String> value){
        if (isParamInValid(key))
            return value;

        try {
            return (ArrayList<String>)params.get(key);
        } catch (ClassCastException e) {
            e.printStackTrace();
        }

        return value;
    }

    public JSONObject getObjectParam(String key, JSONObject value) {
        if (isParamInValid(key))
            return value;

        return (JSONObject) params.get(key);
    }

    public ArrayList<String> getStringListParam(String key){
        return getStringListParam(key, new ArrayList<>(0));
    }

    public int getBeginLine(){
        return getIntegerParam(getBeginLineKey(), 1);
    }

    public int getEndLine(){
        return getIntegerParam(getEndLineKey(), 1);
    }

    public boolean isNested(){
        return getBooleanParam(getNestedKey(), false);
    }

    public String getStripPrefix(){
        return getStringParam(KEY_STRIP_PREFIX, "");
    }

    public String getStripSuffix(){
        return getStringParam(KEY_STRIP_SUFFIX, "");
    }

    public boolean needStripQuote(){
        return getBooleanParam(KEY_STRIP_QUOTE, false);
    }

    public String getFieldSeparator() {
        return getValidStringParam(getFieldSepKey(), getDefaultSep());
    }

    public JSONObject getTransform() {
        return getObjectParam(KEY_TRANSFORM, new JSONObject());
    }
}
