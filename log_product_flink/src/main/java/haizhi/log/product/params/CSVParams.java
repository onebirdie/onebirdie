package haizhi.log.product.params;

import org.json.simple.JSONObject;

import java.util.ArrayList;

public class CSVParams extends BaseParams {
    public static final String KEY_CSV_FIELDS = "csv_fields";

    public CSVParams(JSONObject jsonObject) {
        super(jsonObject);
    }

    public CSVParams(String jsonStr) {
        super(jsonStr);
    }

    @Override
    protected String getFieldSepKey() {
        return "csv_separator";
    }

    public ArrayList<String> getCsvFields() {
        return getStringListParam(KEY_CSV_FIELDS);
    }
}
