package haizhi.log.product.params;

import org.json.simple.JSONObject;

public class JsonParams extends BaseParams {
    public JsonParams(JSONObject jsonObject) {
        super(jsonObject);
    }

    public JsonParams(String jsonStr) {
        super(jsonStr);
    }
}
