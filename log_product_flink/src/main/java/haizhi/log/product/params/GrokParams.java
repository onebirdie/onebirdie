package haizhi.log.product.params;

import org.json.simple.JSONObject;

public class GrokParams extends BaseParams {
    public static final String KEY_REGEX= "regex";

    public GrokParams(JSONObject jsonObject) {
        super(jsonObject);
    }

    public GrokParams(String jsonStr) {
        super(jsonStr);
    }

    public String getRegex() {
        return getValidStringParam(KEY_REGEX, "\\s+");
    }
}
