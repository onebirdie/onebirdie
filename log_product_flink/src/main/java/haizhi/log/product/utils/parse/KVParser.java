package haizhi.log.product.utils.parse;

import com.google.common.base.Splitter;
import org.json.simple.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ysy on 17/7/11 23:52.
 */

public class KVParser extends Parser{
    private String sep;
    private String kvSep;
    private boolean ignoreQuote;
    private String prefixIgnored;

    public KVParser(JSONObject rule) {
        super(rule);
        loadRule(rule);
    }

    public KVParser (Integer id, Integer boundId, String tag, Integer logTypeId, JSONObject rule) {
        super(id, boundId, tag, logTypeId, rule);
        loadRule(rule);
    }

    private void loadRule(JSONObject rule) {
        this.sep = String.valueOf(rule.get("field_separator"));
        if (this.sep == null || "".equals(this.sep))
            this.sep = "\\s+";

        this.kvSep = String.valueOf(rule.get("kv_separator"));
        if (this.kvSep == null || "".equals(this.kvSep))
            this.kvSep = "\\s+";
        this.ignoreQuote = Boolean.valueOf(String.valueOf(rule.get("ignore_quote")));
        if (! rule.containsKey("no_prefix_rule"))
            this.prefixIgnored = "";
        else
            this.prefixIgnored = String.valueOf(rule.get("no_prefix_rule"));
    }

    public HashMap<String, Object> parse (String log)  {
//        prefixIgnored = "\\d+-\\d+-\\d+\\s+\\d+:\\d+:\\d+";
//        System.out.println("prefixIgnored " + prefixIgnored);
        if (prefixIgnored.length() > 0)
            log = log.replaceFirst("^" + prefixIgnored, "");
//        System.out.println(log);
//        System.out.println("sep " + sep);
//        System.out.println("kvSep " + kvSep);
//        System.out.println("ignore quote" + ignoreQuote);
        HashMap<String, Object> result = new HashMap<>();
        try {
            Map<String, String> map = Splitter
                    .onPattern(this.sep)
                    .trimResults()
                    .omitEmptyStrings()
                    .withKeyValueSeparator(Splitter.onPattern(kvSep))
//                    .withKeyValueSeparator(kvSep)
                    .split(log);

            for (Map.Entry<String, String> entry : map.entrySet()) {
                String[] keys = entry.getKey().split("\\s");
                String key = keys[keys.length - 1];
                String value = entry.getValue().trim();
                if (ignoreQuote) {
                    key = removeQuote(key);
                    value = removeQuote(value);
                }
                result.put(key, value);
//            System.out.println(key + " " + entry.getValue());
            }
            return result;
        } catch (Exception e) {
//            System.out.println("kv parser error " + e.getMessage());
            return result;
//            return null;
        }
    }

    private String removeQuote(String str) {
        String s = str.replaceAll("^['\"]", "");
        return s.replaceAll("['\"]$", "");
    }
}
