package haizhi.log.product.utils.mysql;

import java.sql.Connection;
import java.sql.Statement;

/**
 * Created by ysy on 18/3/28 17:34.
 */

public class ConnPack {
    private Connection conn;
    private Statement stmt;

    public ConnPack(Connection conn, Statement stmt) {
        this.conn = conn;
        this.stmt = stmt;
    }

    public Connection getConn() {
        return conn;
    }

    public Statement getStmt() {
        return stmt;
    }

    public void close() throws Exception {
        stmt.close();
        conn.close();
    }
}
