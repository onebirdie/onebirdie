package haizhi.log.product.utils.parse;

import haizhi.log.product.utils.ip.IPOperator;
import haizhi.log.product.utils.mysql.MysqlUtils;
import org.json.simple.JSONObject;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by ysy on 17/8/4 14:16.
 */

public class IPParser {

    private Pattern ipPattern;
    private IPOperator ipOperator;
    private MysqlUtils mysql;

    public IPParser(Map conf) {
        ipPattern = Pattern.compile("^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$");
        ipPattern.getClass().getName();
        ipOperator = new IPOperator();

        try {
            mysql = new MysqlUtils(conf);
        } catch (Exception e0) {
            throw new RuntimeException(e0);
        }
    }

    public void updateIPInfo(String key, String value, JSONObject jsonObject) {
        if (ipPattern.matcher(value).matches()) {
//            System.out.println("IP value " + value);
            HashMap<String, Object> location = queryLocation(value);
            System.out.println("IP location " + location);
            updateLocation(jsonObject, location, key);
            String operator = ipOperator.parse(value);
            updateOperator(jsonObject, operator, key);
        }
    }

    private HashMap<String, Object> queryLocation(String ip) {
        String[] ip_parts = ip.split("\\.");
        long ip_num = 0L;
        for (int i = 0; i < 4; i++ ) {
            ip_num += Long.parseLong(ip_parts[i]) << (24 - 8 * i);
        }
//        System.out.println("ip_num " + ip_num);
        HashMap<String, Object> location = new HashMap<>();
        try {
            ResultSet result = mysql.executeQuery("select province, city, county, code from ip_code where id = (select max(id) from ip_code where id < " + ip_num + ")");
            while (result.next()) {
                location.put("province", result.getString("province"));
                location.put("city", result.getString("city"));
                location.put("county", result.getString("county"));
                location.put("code", result.getInt("code"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("IPParser mysql query error " + e.getMessage());
//            log.error("query mysql error " + e.getMessage());
//            log.catching(e);
        }
        return location;
    }

    private void updateLocation(JSONObject jsonObject, HashMap<String, Object> location, String key) {
        if (location == null || location.size() == 0)
            return;
        String province = String.valueOf(location.get("province"));
        if (province != null && ! "".equals(province))
            jsonObject.put(key + "_province", province);
        String city = String.valueOf(location.get("city"));
        if (city != null && ! "".equals(city))
            jsonObject.put(key + "_city", city);
        String county = String.valueOf(location.get("county"));
        if (county != null && ! "".equals(county))
            jsonObject.put(key + "_county", county);
        int code = Integer.parseInt(String.valueOf(location.get("code")));
        if (code != 0)
            jsonObject.put(key + "_code", code);
    }

    private void updateOperator(JSONObject jsonObject, String operator, String key) {
        if (operator == null || "".equals(operator.trim()))
            return;
        jsonObject.put(key + "_operator", operator);
    }
}
