package haizhi.log.product.utils.ts;

import java.util.ArrayList;

/**
 * Created by ysy on 17/6/26 15:01.
 */

public class YMDHelper {
    private ArrayList<Integer> self;
    Boolean centurySpecified;
    private Integer now_year;

    YMDHelper() {
        self = new ArrayList<>();
        centurySpecified = false;
        now_year = 2018;
    }

    Integer size() {
        return self.size();
    }

    void clear() {
        self = new ArrayList<>();
        centurySpecified = null;
    }

    void append(String val) {
        append(Integer.parseInt(val));
    }

    void append(int val) {
        if (val == 0 || (val >= 32 && val <= now_year - 3) || val >= now_year + 3)
            return;

        if (val > 100)
            centurySpecified = true;

        self.add(val);
    }

    void popHead() {
        self.remove(0);
    }

    YMD resolveYmd(Integer monthStrIdx) {
        Integer lenYmd = self.size();
        YMD ymd = new YMD();

        if (lenYmd == 1 || (monthStrIdx != -1 && lenYmd == 2)) {
            if (monthStrIdx != -1) {
                ymd.month = self.get(monthStrIdx);
                self.remove((int)monthStrIdx);

            }
            if (lenYmd > 1 || monthStrIdx == -1) {
                if (self.get(0) <= 31)
                    ymd.day = self.get(0);
                else if (self.get(0) >= 2000)
                    ymd.year = self.get(0);
            }
        }
        else if (lenYmd == 2) {
            if (self.get(0) > 31) {
                ymd.year = self.get(0);
                ymd.month = self.get(1);
            }
            else if (self.get(1) > 31) {
                ymd.month = self.get(0);
                ymd.year = self.get(1);
            }
            else {
                ymd.month = self.get(0);
                ymd.day = self.get(1);
            }
        }
        else if (lenYmd >= 3) {
            while (self.size() > 3) {
//                self.remove(self.size() - 1);
                self.remove(0);
                monthStrIdx -= 1;
            }
            if (monthStrIdx == 0) {
                ymd.month = self.get(0);
                ymd.day = self.get(1);
                ymd.year = self.get(2);
            }
            else if (monthStrIdx == 1) {
                ymd.month = self.get(1);
                if (self.get(0) > 31) {
                    ymd.year = self.get(0);
                    ymd.day = self.get(2);
                }
                else {
                    ymd.day = self.get(0);
                    ymd.year = self.get(2);
                }
            }
            else if (monthStrIdx == 2) {
                ymd.month = self.get(2);
                if (self.get(1) > 31) {
                    ymd.day = self.get(0);
                    ymd.year = self.get(1);
                }
                else {
                    ymd.year = self.get(0);
                    ymd.day = self.get(1);
                }
            }
            else {
                if (self.get(0) > 31) {
                    ymd.year = self.get(0);
                    ymd.month = self.get(1);
                    ymd.day = self.get(2);
                }
                else if (self.get(0) > 12) {
                    ymd.day = self.get(2);
                    ymd.month = self.get(1);
                    ymd.year = self.get(0);
                }
                else {
                    ymd.month = self.get(0);
                    ymd.day = self.get(1);
                    ymd.year = self.get(2);
                }
            }
        }
        return ymd;
    }

    @Override
    public String toString() {
        String str = "";
        for (Integer aSelf : self) str += aSelf + " ";
        return str;
    }
}
