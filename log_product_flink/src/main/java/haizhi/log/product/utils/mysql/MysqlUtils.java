package haizhi.log.product.utils.mysql;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ysy on 17/6/26 11:58.
 */

public class MysqlUtils {
//    private Connection conn;
//    private Statement stmt;
    private String jdbcUrl;
    private String user;
    private String password;

    private String urlPrefix;
    private final String urlSuffix = "?autoReconnect=true&useSSL=false";

    private HashMap<String, ConnPack> connectionMap = new HashMap<>();
    private String defaultDatabase;

    // TODO 三种初始化方式太墨迹了，得改
    public MysqlUtils(Map conf) throws Exception {
        String host = String.valueOf(conf.get("mysql.host"));
        String port = String.valueOf(conf.get("mysql.port"));
        String database = String.valueOf(conf.get("mysql.database"));
        String user = String.valueOf(conf.get("mysql.user"));
        String password = String.valueOf(conf.get("mysql.password"));

        this.urlPrefix = "jdbc:mysql://" + host + ":" + port + "/";
        this.jdbcUrl = urlPrefix + database + urlSuffix;
        this.user = user;
        this.password = password;

        this.defaultDatabase = database;
        connect(database);
    }

    public MysqlUtils(String domain, Map conf) throws Exception {
        String host = String.valueOf(conf.get("mysql.host"));
        String port = String.valueOf(conf.get("mysql.port"));
//        String database = String.valueOf(conf.get("mysql.database"));
        String user = String.valueOf(conf.get("mysql.user"));
        String password = String.valueOf(conf.get("mysql.password"));

        String database = convertDomainToDB(domain);
        this.urlPrefix = "jdbc:mysql://" + host + ":" + port + "/";
        this.jdbcUrl = urlPrefix + database + urlSuffix;
        this.user = user;
        this.password = password;

        this.defaultDatabase = database;
        connect(database);
    }

    public MysqlUtils(String host, String port, String database, String user, String password) throws Exception {
//        this.jdbcUrl = "jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=true&useSSL=false";
        this.urlPrefix = "jdbc:mysql://" + host + ":" + port + "/";
        this.jdbcUrl = urlPrefix + database + urlSuffix;
        this.user = user;
        this.password = password;

        this.defaultDatabase = database;
        connect(database);
    }

    private void connectByDomain(String domain) throws Exception {
        String database = convertDomainToDB(domain);
        connect(database);
    }

    private void connect(String database) throws Exception {
        String jdbcUrl = getJdbcUrlByDB(database);
        try {
            Connection conn = DriverManager.getConnection(jdbcUrl, user, password);
            Statement stmt = conn.createStatement();
            connectionMap.put(database, new ConnPack(conn, stmt));
        } catch (SQLException e0) {
            throw new RuntimeException(e0);
        }
    }

    public ResultSet executeQueryByDomain(String domain, String query) throws Exception {
        String database = convertDomainToDB(domain);
        return executeQueryByDB(database, query);
    }

    private ResultSet executeQueryByDB(String database, String query) throws Exception {
        if (! connectionMap.containsKey(database)) {
            connect(database);
        }
        try {
            return executeQuery(connectionMap.get(database), query);
        } catch (Exception e) {
            connect(database);
            return executeQuery(connectionMap.get(database), query);
        }
    }

    public ResultSet executeQuery(String query) throws Exception {
        return executeQueryByDB(defaultDatabase, query);
    }

    private ResultSet executeQuery(ConnPack connPack, String query) throws Exception {
        Statement stmt = connPack.getStmt();
        try {
            return stmt.executeQuery(query);
        } catch (Exception e1) {
            throw new RuntimeException(e1);
        }
    }

    private String getJdbcUrlByDB(String database) {
        return urlPrefix + database + urlSuffix;
    }

    private String convertDomainToDB(String domain) {
        return domain + "_log_product";
    }

//        try {
//            return stmt.executeQuery(query);
//        } catch (Exception e1) {
//            try {
//                connect();
//                return stmt.executeQuery(query);
//            } catch (Exception e2) {
//                throw new RuntimeException(e2);
//            }
//        }
//    }

//    public ResultSet executeQuery(String query) throws Exception {
//        try {
//            return stmt.executeQuery(query);
//        } catch (Exception e1) {
//            try {
//                connect();
//                return stmt.executeQuery(query);
//            } catch (Exception e2) {
//                throw new RuntimeException(e2);
//            }
//        }
//    }

    public void cleanup() {
        for (String database : connectionMap.keySet()) {
            ConnPack connPack = connectionMap.get(database);
            try {
                connPack.close();
            } catch (Exception e) {
            }
        }
    }
}

