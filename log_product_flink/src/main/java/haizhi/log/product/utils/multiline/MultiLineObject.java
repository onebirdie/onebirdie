package haizhi.log.product.utils.multiline;

import haizhi.log.product.utils.LogData;
import org.apache.storm.tuple.Tuple;

/**
 * Created by ysy on 17/6/26 11:55.
 */

public class MultiLineObject {
//    public String raw;
//    public Long ts;
    private LogData logData;
    public Tuple tuple;
    public int lineNum;
    private Long updateTs;

    public MultiLineObject(LogData logData, Tuple tuple) {
        this.logData = logData;
        this.tuple = tuple;
        this.lineNum = 1;
        this.updateTs = System.currentTimeMillis();
    }

//    public MultiLineObject(String raw, Long ts, Tuple tuple) {
//        this.raw = raw;
//        this.ts = ts;
//        this.tuple = tuple;
//        this.lineNum = 1;
//        this.updateTs = System.currentTimeMillis();
//    }

    public LogData getLogData() {
        return this.logData;
    }

//    public String getToken() {
//        return this.logData.getToken();
//    }

//    public String getRaw() {
//        return this.logData.getRaw();
//    }

    public int getLineNum() {
        return this.lineNum;
    }

    public Tuple getTuple() {
        return this.tuple;
    }

    public void insertLine(LogData logData) {
        this.logData.appendRaw(logData);
        this.lineNum += 1;
    }

//    public void insertLine(String raw) {
//        this.raw += "\n" + raw;
//        lineNum += 1;
//        updateTs = System.currentTimeMillis();
//    }

//    public void clear() {
//        this.raw = null;
//        this.ts = null;
//        this.tuple = null;
//        this.lineNum = 0;
//    }

    private Boolean isTimeout(Long timeoutSec) {
        return System.currentTimeMillis() - updateTs > timeoutSec * 1000;
    }

    public Boolean isOverSize(Integer maxSize) {
        return lineNum >= maxSize;
    }

    public Boolean canFlush(Long timeoutSec, Integer maxSize) {
        return lineNum > 0 && (isTimeout(timeoutSec) || isOverSize(maxSize));
    }
}
