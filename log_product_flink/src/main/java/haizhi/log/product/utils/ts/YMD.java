package haizhi.log.product.utils.ts;

/**
 * Created by ysy on 17/6/26 15:00.
 */

public class YMD {
    Integer year;
    Integer month;
    Integer day;

    YMD() {
        year = null;
        month = null;
        day = null;
    }
    public YMD(Integer year, Integer month, Integer day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    @Override
    public String toString() {
        return year + "-" + month + "-"  + day;
    }
}

