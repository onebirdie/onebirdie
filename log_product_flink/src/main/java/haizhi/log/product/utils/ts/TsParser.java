package haizhi.log.product.utils.ts;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * Created by ysy on 17/6/26 15:02.
 */

public class TsParser {

    private ZoneId zoneId = ZoneId.systemDefault();
    private final ParserInfo info = new ParserInfo();
    private final Pattern pattern = Pattern.compile("(?<=[.,])|(?=[.,])");
    private final int timeStrMaxLength = 224;
    private final Pattern linuxPermissionPattern = Pattern.compile("^-[rwx-]{9}\\s");

    public TsParser() {
    }

    public Long parseTs(String timeStr) {
        return parseTs(timeStr, 86400L * 730L, 3600L);
    }

    public Long parseTs(String timeStr, long lowerBound, long upperBound) {
        timeStr = timeStr.trim();

        if (canQuickPass(timeStr)){
            return null;
        }

        // 为处理 shell ll 命令后的结果，强制不解析出时间戳
        if (linuxPermissionPattern.matcher(timeStr).find()) {
            return null;
        }

        // 为处理 2时21分34秒859 这种日期格式
        timeStr = timeStr.replaceFirst("时", ":");
        timeStr = timeStr.replaceFirst("分", ":");
        timeStr = timeStr.replaceFirst("秒", ",");

        // 临时为绍兴银行上的逻辑
        timeStr = timeStr.replaceFirst("二月", "feb");
        timeStr = timeStr.replaceFirst("三月", "mar");

//        System.out.println("timeStr " + timeStr);

//        if (timeStr.startsWith("<")) {
////            System.out.println("bingo");
//            timeStr = timeStr.replaceFirst("<\\d{1,3}>\\d*\\d:", "");
//        }

//        System.out.println("timeStr " + timeStr);

        int len = timeStr.length();
        if (len > timeStrMaxLength) {
            timeStr = timeStr.substring(0, timeStrMaxLength);
        }

        try {
            timeStr = timeStr.replaceAll("\\s+", " ");
            timeStr = timeStr.toLowerCase();
            Result result = parse(timeStr);
//            System.out.println(result);
            if (result == null || (result.hour == null && result.minute == null && result.second == null)) {
                return null;
            }

            LocalDateTime defaultTime = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault());
            defaultTime = result.replace(defaultTime);

            Long ts = defaultTime.atZone(zoneId).toEpochSecond();

            Long nowTs = System.currentTimeMillis() / 1000;
            if (ts > (nowTs + upperBound) || ts < (nowTs - lowerBound)) {
                return null;
            }
            if (result.microsecond != null) {
                return ts * 1000L + result.microsecond / 1000;
            } else {
                return ts * 1000L;
            }
        } catch (Exception e0) {
            return null;
        }

    }

    private boolean canQuickPass(String timeStr){
        return  maybeXml(timeStr) || maybeJson(timeStr);
    }

    private boolean maybeXml(String timeStr){
        return timeStr.startsWith("<") && timeStr.endsWith(">");
    }

    private boolean maybeJson(String timeStr){
        return timeStr.startsWith("{") && timeStr.endsWith("}");
    }

    private boolean isUpper(final char c){
        return 'A' <= c && c <= 'Z';
    }

    private boolean isLower(final char c) {
        return 'a' <= c && c <= 'z';
    }

    private boolean isDigit(final char c) {
        return '0' <= c && c <= '9';
    }

    private ArrayList<String> splitTokens(final String s) {

        final int sLen = s.length();
        ArrayList<String> tokens = new ArrayList<>();
        int idx = 0;
        StringBuilder token = new StringBuilder(4);
        int token_length;
        String tok;
        char nextChar;
        boolean seenLetters;
        String[] splits;
        char last_char;

        while (true) {
            seenLetters = false;
            token.setLength(0);
            int state = 0;

            label:
            while (idx < sLen) {
                nextChar = s.charAt(idx ++);

                switch (state) {
                    case 0:
                        token.append(nextChar);
                        if (isLower(nextChar))
                            state = 1;
                        else if (isDigit(nextChar))
                            state = 2;
                        else if (nextChar == ' ') {
                            token.setLength(0);
                            token.append(' ');
                            break label;
                        }
                        else
                            break label;
                        break;
                    case 1:
                        seenLetters = true;
                        if (isLower(nextChar))
                            token.append(nextChar);
                        else if (nextChar == '.') {
                            token.append(nextChar);
                            state = 3;
                        } else {
                            idx--;
                            break label;
                        }
                        break;
                    case 2:
                        if (isDigit(nextChar)) {
                            token.append(nextChar);
                        } else if (nextChar != '.' && nextChar != ',') {
                            idx--;
                            break label;
                        } else {
                            token.append(".");
                            state = 4;
                        }
                        break;
                    case 3:
                        seenLetters = true;
                        if (isLower(nextChar) || nextChar == '.')
                            token.append(nextChar);
                        else if (isDigit(nextChar) && token.charAt(token.length() - 1) == '.') {
                            token.append(nextChar);
                            state = 4;
                        } else {
                            idx--;
                            break label;
                        }
                        break;
                    case 4:
                        if (isDigit(nextChar) || nextChar == '.')
                            token.append(nextChar);
                        else if (isLower(nextChar) && token.charAt(token.length() - 1) == '.') {
                            token.append(nextChar);
                            state = 3;
                        } else {
                            idx--;
                            break label;
                        }
                        break;
                }
            }

            token_length = token.length();

            if (token_length == 0)
                break;

            if (state >= 3) {
                if (seenLetters) {
                    tok = token.toString();
                    splits = pattern.split(tok);
                    tokens.addAll(Arrays.asList(splits));
                    continue;
                }
                last_char = token.charAt(token_length - 1);

                if (last_char == '.' || last_char == ',') {
                    tok = token.toString();
                    splits = pattern.split(tok);
                    tokens.addAll(Arrays.asList(splits));
                    continue;
                }

                int dotCount = 0;
                for( int i = 0; i < token_length; i++) {
                    if( s.charAt(i) == '.' )
                        dotCount ++;
                }
                if (dotCount > 1) {
                    tok = token.toString();
                    splits = pattern.split(tok);
                    tokens.addAll(Arrays.asList(splits));
                    continue;
                }
            }

//            tok = token.toString();

            tokens.add(token.toString());
        }
        return tokens;
    }

    // 比 Integer.parseInt 更快的写法
    private int parseInt(final String s) {

        // Check for a sign.
        int num  = 0;
        int sign = -1;
        final int len  = s.length();
        final char ch  = s.charAt(0);
        if ( ch == '-' )
        {
            if ( len == 1 )
                throw new NumberFormatException();
            sign = 1;
        }
        else
        {
            final int d = ch - '0';
            if ( d < 0 || d > 9 )
                throw new NumberFormatException();
            num = -d;
        }

        // Build the number.
        final int max = (sign == -1) ?
                -Integer.MAX_VALUE : Integer.MIN_VALUE;
        final int multmax = max / 10;
        int i = 1;
        while ( i < len )
        {
            int d = s.charAt(i++) - '0';
            if ( d < 0 || d > 9 )
                throw new NumberFormatException();
            if ( num < multmax )
                throw new NumberFormatException();
            num *= 10;
            if ( num < (max+d) )
                throw new NumberFormatException();
            num -= d;
        }

        return sign * num;
    }

    // 比 parseInt 更快的写法, 负数和异常时均返回 -1
    private int parseInt2(final String s) {

        final int len  = s.length();
        int num = 0;
        int d;
        int i = 0;

        while (i < len)
        {
            d = s.charAt(i) - 48;
            if (d < 0 || d > 9) {
                return -1;
            }
            num *= 10;
            num += d;
            i ++;
        }

        return num;
    }

    private Result parse(final String timeStr) {
        Result res = new Result();

        ArrayList<String> l = splitTokens(timeStr);

        try {
            YMDHelper ymd = new YMDHelper();
            int monthStrIdx = -1;
            final int lenl = l.size();
            int i = 0;
            Boolean hms = false;
            int ymdPos = 0;
            int hmsPos = 0;
            while (i < lenl) {
                if (ymdPos == 0 && ymd.size() >= 3) {
                    ymdPos = i - 1;
                }
                if (res.hour != null && res.minute != null && res.second != null)
                {
                    if (ymd.size() >= 2) {
                        break;
                    }
                    if (res.hour != null) {
                        if (!hms) {
                            hms = true;
                            hmsPos = i - 1;
                        } else {
                            if (i >= hmsPos + 2)
                                break;
                        }
                    }
                }

                String valueRepr = l.get(i);
                int value;
                try {
                    value = parseInt2(valueRepr);
                } catch (NumberFormatException e0){
                    value = -1;
                }

                if (value >= 0) {
                    // token is number

                    int lenli = valueRepr.length();
                    i ++;

                    if (ymd.size() == 3 && (lenli == 2 || lenli == 4) && res.hour != null
                            && (i >= lenl || (!l.get(i).equals(":") && info.hms(l.get(i)) == null))) {
                        // 19990101T23[59]

                        res.hour = parseInt(valueRepr.substring(0, 2));
                        if (lenli == 4)
                            res.minute = parseInt(valueRepr.substring(2));
                    }
                    else if ((lenli == 6 && ymd.size() > 2) || ((lenli == 8 || lenli == 10 || lenli == 13) && valueRepr.indexOf(".") == 6)) {
                        // YYMMDD or HHMMSS[.ss]

                        if (ymd.size() == 0 && !valueRepr.contains(".")) {
                            ymd.append(valueRepr.substring(0, 2));
                            ymd.append(valueRepr.substring(2, 4));
                            ymd.append(valueRepr.substring(4));
                        }
                        else {
                            res.hour = parseInt(valueRepr.substring(0, 2));
                            res.minute = parseInt(valueRepr.substring(2, 4));
                            res.second = parseSecond(valueRepr.substring(4));
                            res.microsecond = parseMicroSecond(valueRepr.substring(4));
                        }
                    }
                    else if ((lenli == 6 || lenli == 4) && ymd.size() == 0 && lenl >= i + 2 && ":".equals(l.get(i)) && l.get(i + 1).length() == 6) {
                        // [YY]MMDD:HHMMSS[:sss]
                        if (lenli == 6) {
                            ymd.append(valueRepr.substring(0, 2));
                            ymd.append(valueRepr.substring(2, 4));
                            ymd.append(valueRepr.substring(4, 6));
                        } else {
                            ymd.append(valueRepr.substring(0, 2));
                            ymd.append(valueRepr.substring(2, 4));
                        }

                        valueRepr = l.get(i + 1);
                        res.hour = parseInt(valueRepr.substring(0, 2));
                        res.minute = parseInt(valueRepr.substring(2, 4));
                        res.second = parseInt(valueRepr.substring(4, 6));

                        i += 2;
                        if (lenl >= i + 2 && ":".equals(l.get(i))) {
                            res.microsecond = parseMicroSecond("." + l.get(i + 1));
                            i += 2;
                        }
                    }
//                    else if (lenli == 8 || lenli == 12 || lenli == 14) {
                    // 不再支持 YYYYMMDDHHMM[SS] 这种很可能出现在多行字段值中的格式
                    else if (lenli == 8) {
                        // YYYYMMDD

                        ymd.append(valueRepr.substring(0, 4));
                        ymd.append(valueRepr.substring(4, 6));
                        ymd.append(valueRepr.substring(6, 8));

                        // 需检查 ymd 是否添加成功
//                        if (ymd.size() == 3 && lenli > 8) {
//                            res.hour = parseInt(valueRepr.substring(8, 10));
//                            res.minute = parseInt(valueRepr.substring(10, 12));
//                            if (lenli == 14)
//                                res.second = parseInt(valueRepr.substring(12));
//                        }
                    }
                    else if (i + 1 < lenl && l.get(i).equals(":")) {
                        // HH:MM[:SS[.ss]]

                        if (value <= 59) {
                            res.hour = value;
                            i ++;
                            value = parseInt(l.get(i));
                            res.minute = value;

                            i ++;
                            if (i < lenl && l.get(i).equals(":")) {

                                // HH:MM:SS:sss[sss]
                                if ((lenl >= i + 4 && ":".equals(l.get(i + 2)) && (l.get(i + 3).length() == 3 || l.get(i + 3).length() == 6))
                                        || lenl >= i + 4 && " ".equals(l.get(i + 2)) && l.get(i + 3).length() == 6 && l.get(i + 1).length() == 2) {
                                    int micro_value = parseInt2(l.get(i + 3));
                                    if (micro_value == -1) {
                                        res.second = parseSecond(l.get(i + 1));
                                        i += 4;
                                    } else {
                                        String valueStr = l.get(i + 1) + "." + l.get(i + 3);
                                        res.second = parseSecond(valueStr);
                                        res.microsecond = parseMicroSecond(valueStr);
                                        i += 4;
                                    }
                                }
                                else {
                                    res.second = parseSecond(l.get(i + 1));
                                    res.microsecond = parseMicroSecond(l.get(i + 1));
                                    i += 2;
                                }
                            }
                        }
//                        else {
//                            i ++;
//                            continue;
//                        }
                    }
                    else if (info.jump.contains(l.get(i)) || i >= lenl) {
                        ymd.append(value);
                        i ++;
                    }
                    else
                        i++;
                    continue;
                }
                else {
                    if (!info.tokens.contains(valueRepr)) {
                        i++;
                        continue;
                    }

                    // check weekday
                    if (info.weekDayMap.containsKey(valueRepr)) {
                        res.weekday = info.weekDayMap.get(valueRepr);
                        i++;
                        continue;
                    }

                    // check month name
                    Integer v = info.month(valueRepr);
                    if (v != null) {
                        ymd.append(v);
                        assert monthStrIdx == -1;
                        monthStrIdx = ymd.size() - 1;

                        i++;
                        if (i < lenl && (l.get(i).equals("-") || l.get(i).equals("/"))) {
                            // Jan-01[-99]
                            String sep = l.get(i);
                            i++;
                            ymd.append(l.get(i));
                            i++;

                            if (i < lenl && l.get(i).equals(sep)) {
                                // Jan-01-99
                                i++;
                                ymd.append(l.get(i));
                                i++;
                            }
                        }
                        continue;
                    }
                }
                i ++;
            }
            YMD tmpYmd = ymd.resolveYmd(monthStrIdx);
            if (tmpYmd.year != null) {
                res.year = tmpYmd.year;
                res.century_specified = ymd.centurySpecified;
            }

            if (tmpYmd.month != null)
                res.month = tmpYmd.month;

            if (tmpYmd.day != null)
                res.day = tmpYmd.day;

        } catch (Exception e1) {
//            System.out.println(e1.getMessage());
            return null;
        }

        info.validate(res);
        return res;
    }

    private Integer parseSecond(String value) {
        return (int) Float.parseFloat(value);
    }

    private Integer parseMicroSecond(String value) {
//        System.out.println("parse micro second " + value);
        if (value.contains(".")) {
            Float v = Float.parseFloat(value);
            return (int) (v * 1000000 % 1000000);
        }
        else
            return 0;
    }
}

