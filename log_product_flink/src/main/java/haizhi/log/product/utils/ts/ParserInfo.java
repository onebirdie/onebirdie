package haizhi.log.product.utils.ts;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by ysy on 17/6/26 14:56.
 */

public class ParserInfo {
    public final HashSet<String> jump = new HashSet<String>() {{
        add(" ");
        add(".");
        add(",");
        add(";");
        add("-");
        add("/");
        add("'");
        add("at");
        add("on");
        add("and");
        add("ad");
        add("m");
        add("t");
        add("of");
        add("st");
        add("nd");
        add("rd");
        add("th");
        add("[");
        add("]");
    }};
    public HashSet<String> tokens;
    HashMap<String, Integer> weekDayMap;
    private HashMap<String, Integer> monthMap;
    private HashMap<String, Integer> hmsMap;

//    public Boolean dayFirst = false;
//    public Boolean yearFirst = false;

    private Integer century;

    public ParserInfo() {
//        Integer year = LocalDate.now().getYear();
        century = 2000;
//        initJump();
        initWeekDayMap();
        initMonthMap();
        initHmsMap();
        initTokens();
    }

//    private void initJump() {
//        jump = new HashSet<String>() {{
//            add(" ");
//            add(".");
//            add(",");
//            add(";");
//            add("-");
//            add("/");
//            add("'");
//            add("at");
//            add("on");
//            add("and");
//            add("ad");
//            add("m");
//            add("t");
//            add("of");
//            add("st");
//            add("nd");
//            add("rd");
//            add("th");
//            add("[");
//            add("]");
//        }};
//    }

    private void initWeekDayMap() {
        weekDayMap = new HashMap<String, Integer>() {{
            put("mon", 0);
            put("monday", 0);
            put("tue", 1);
            put("tuesday", 1);
            put("wed", 2);
            put("wednesday", 2);
            put("thu", 3);
            put("thursday", 3);
            put("fri", 4);
            put("friday", 4);
            put("sat", 5);
            put("saturday", 5);
            put("sun", 6);
            put("sunday", 6);
        }};
    }

    private void initMonthMap() {
        monthMap = new HashMap<String, Integer>() {{
            put("jan", 0);
            put("january", 0);
            put("feb", 1);
            put("february", 1);
            put("mar", 2);
            put("march", 2);
            put("apr", 3);
            put("april", 3);
            put("may", 4);
            put("jun", 5);
            put("june", 5);
            put("jul", 6);
            put("july", 6);
            put("aug", 7);
            put("august", 7);
            put("sep", 8);
            put("sept", 8);
            put("september", 8);
            put("oct", 9);
            put("october", 9);
            put("nov", 10);
            put("november", 10);
            put("dec", 11);
            put("december", 11);
        }};
    }

    private void initHmsMap() {
        hmsMap = new HashMap<String, Integer>() {{
            put("h", 0);
            put("hour", 0);
            put("hours", 0);
            put("m", 1);
            put("minute", 1);
            put("minutes", 1);
            put("s", 2);
            put("second", 2);
            put("seconds", 2);
        }};
    }

    private void initTokens() {
        tokens = new HashSet<>();
        tokens.addAll(weekDayMap.keySet());
        tokens.addAll(monthMap.keySet());
        tokens.addAll(hmsMap.keySet());
    }

    private Integer convertYear(Integer year, Boolean centurySpecified) {
        if (!centurySpecified)
            year += this.century;
        return year;
    }

    void validate(Result result) {
        if (result.year != null)
            result.year = convertYear(result.year, result.century_specified);
    }

    public Integer weekday(String name) {
        return weekDayMap.get(name);
    }

    Integer month(String name) {
        if (! monthMap.containsKey(name))
            return null;
        else
            return monthMap.get(name) + 1;
    }

    Integer hms(String name) {
        return hmsMap.get(name);
    }

    public static void main(String[] args) {
//        long t0 = new Date().getTime();

//        System.out.println("use " + String.valueOf(new Date().getTime() - t0) + " ms");
    }
}
