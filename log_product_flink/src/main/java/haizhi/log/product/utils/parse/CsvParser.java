package haizhi.log.product.utils.parse;

import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ysy on 17/7/12 15:50.
 */

public class CsvParser extends Parser{
    private ArrayList<String> fields;
    private String sep;

    public CsvParser(JSONObject rule) {
        super(rule);
        loadRule(rule);
    }

    public CsvParser(Integer id, Integer boundId, String tag, Integer logTypeId, JSONObject rule) {
        super(id, boundId, tag, logTypeId, rule);
        loadRule(rule);
    }

    private void loadRule(JSONObject rule) {
        this.fields = (ArrayList<String>) rule.get("csv_fields");
        this.sep = String.valueOf(rule.get("csv_separator"));
    }

    public HashMap<String, Object> parse (String log) {

//        System.out.println("sep " + sep);

        List<String> items = Arrays.asList(log.split("(?<!\\\\)" + sep));

        if (items.size() != this.fields.size()) {
            return null;
        }

        HashMap<String, Object> result = new HashMap<>();
        for (int i = 0; i < items.size(); i++) {
            result.put(fields.get(i).trim(), items.get(i));
        }
        return result;
    }
}
