package haizhi.log.product.utils.parse;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ysy on 17/7/17 00:04.
 */

public class Node {
    private Parser parser;
    private Node parent;
    private ArrayList<Node> children;

    public Node(Parser parser) {
        this.parser = parser;
        this.children = new ArrayList<>();
        this.parent = null;
    }

    public void addChild(Node node) {
        children.add(node);
    }

    public void setParent(Node node) {
        parent = node;
    }

    public Node getParent() {
        return parent;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public Integer getBoundId() {
        return parser.getBoundId();
    }

    public String getTag() {
        return parser.getTag();
    }

    public Integer getLogTypeId() {
        return parser.getLogTypeId();
    }

    public Integer getId() {
        return parser.getId();
    }

    public HashMap<String, Object> parse(String log, HashMap<String, Object> oldResult) {
//        System.out.println("node " + getId() + " parse with old result size " + oldResult.size());
        HashMap<String, Object> result;
        try {
            result = parser.parse(log);
            if (result == null || result.size() == 0)
                throw new RuntimeException();
        } catch (Exception e) {
            return null;
        }

//        System.out.println("node " + getId() + " parse result size " + result.size());
        oldResult.putAll(result);
        for (Node node : children) {
            result = node.parse(log, oldResult);
            if (result != null && result.size() > 0) {
                oldResult.putAll(result);
                return result;
            }
        }

        return oldResult;
    }
}
