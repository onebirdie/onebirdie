package haizhi.log.product.utils.parse;

import haizhi.log.product.utils.json.MyJson;
import haizhi.log.product.utils.mysql.MysqlUtils;
import org.json.simple.JSONObject;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ysy on 17/7/16 23:26.
 */

public class ParserForest {
    private MysqlUtils mysql;
    private HashMap<String, ParserTree> treeMap;
    private HashMap<Integer, Node> nodeMap;

    public ParserForest(Map conf) {
        try {
            mysql = new MysqlUtils(conf);
        } catch (Exception e0) {
            throw new RuntimeException(e0);
        }

        treeMap = new HashMap<>();
        nodeMap = new HashMap<>();
//        loadAllRule(null);
//        loadForest();
    }

    public void loadAllRule(Integer ignoreId) {
        String sql = "select id, bound_id, tag, type, log_type_id, rule " +
                "from log_parse_rule " +
                "where disabled=0 and is_deleted=0";
        if (ignoreId != null)
            sql += " and id != " + String.valueOf(ignoreId);
        haizhi.log.product.utils.json.MyJson myJson = new haizhi.log.product.utils.json.MyJson();
        try {
            ResultSet results = mysql.executeQuery(sql);
            while (results.next()) {
                Integer id = results.getInt("id");
                Integer boundId = results.getInt("bound_id");
                String tag = results.getString("tag");
                String type = results.getString("type");
                Integer logTypeId = results.getInt("log_type_id");
                String ruleStr = results.getString("rule");
                JSONObject rule = myJson.safeLoads(ruleStr);

                Parser parser = generateParser(id, boundId, tag, type, logTypeId, rule);
//                parsers.add(parser);
//                parserMap.put(id, parser);
                Node node = new Node(parser);
                nodeMap.put(id, node);
            }
        } catch (Exception e1) {
            throw new RuntimeException(e1);
        }
        System.out.println("load " + String.valueOf(nodeMap.size()) + " rules");
    }

    private Parser generateParser(JSONObject params) {
        Integer id = Integer.valueOf(String.valueOf(params.get("id")));
        Integer boundId;
        if (params.containsKey("bound_id") && params.get("bound_id") != null && ! "".equals(String.valueOf(params.get("bound_id"))))
            boundId = Integer.valueOf(String.valueOf(params.get("bound_id")));
        else
            boundId = null;
//        Integer boundId = Integer.valueOf(String.valueOf(params.get("bound_id")));
        String tag = String.valueOf(params.get("tag"));
        String type = String.valueOf(params.get("type"));
        Integer logTypeId = Integer.valueOf(String.valueOf(params.get("log_type_id")));
        JSONObject rule = (JSONObject) params.get("rule");
        return generateParser(id, boundId, tag, type, logTypeId, rule);
    }

    private Parser generateParser(Integer id, Integer boundId, String tag, String type, Integer logTypeId, JSONObject rule) {
        Parser parser;
        switch (type) {
            case "xml": {
                parser = new XmlParser(id, boundId, tag, logTypeId, rule);
                break;
            }
            case "csv": {
                parser = new CsvParser(id, boundId, tag, logTypeId, rule);
                break;
            }
            case "kv": {
                parser = new KVParser(id, boundId, tag, logTypeId, rule);
                break;
            }
            default: {
                String regex = String.valueOf(rule.get("regex"));
                String transform = String.valueOf(rule.get("transform"));
                parser = new GrokParser(id, boundId, tag, logTypeId, rule, regex, transform);
                break;
            }
        }
        return parser;
    }

    public void loadForest() {
        for (Map.Entry<Integer, Node> entry : nodeMap.entrySet()) {
//            Integer id = entry.getKey();
            Node node = entry.getValue();
            if (node == null)
                continue;
            Integer boundId = node.getBoundId();
            if (boundId != null && boundId != 0) {
                Node parent = nodeMap.get(boundId);
                if (parent == null)
                    continue;
                parent.addChild(node);
                node.setParent(parent);
            }
            else {
                Integer logTypeid = node.getLogTypeId();
                String tag = node.getTag();
                String key = generateKey(logTypeid, tag);
                if (key != null && ! treeMap.containsKey(key)) {
                    treeMap.put(key, new ParserTree(node));
//                    System.out.println("load root node bound tag " + tag);
                }
                if (key == null) {
                    throw new RuntimeException("rule " + node.getId() + " has no boundId or tag");
                }
            }
        }
//        System.out.println("tree keys " + treeMap.keySet());
    }

    private String generateKey(Integer logTypeId, String tag) {
        if (tag == null || "".equals(tag))
            return null;
        else
            return String.valueOf(logTypeId) + "_" + tag;
    }

    public HashMap<String, Object> parse(Integer logTypeId, String tag, String log) {
        String key = generateKey(logTypeId, tag);
        if (key == null || ! treeMap.containsKey(key))
            return null;
        try {
            return treeMap.get(key).parse(log);
        } catch (Exception e) {
            return null;
        }
    }

    public void addNode(JSONObject params) {
        Integer id;
        if (! params.containsKey("id") || params.get("id") == null) {  // 新规则
            id = -1;
            params.put("id", id);
            Parser parser = generateParser(params);
            Node node = new Node(parser);
            nodeMap.put(id, node);
        } else {  // 旧规则
            Parser parser = generateParser(params);
            Node node = new Node(parser);
            id = Integer.valueOf(String.valueOf(params.get("id")));
            nodeMap.put(id, node);
        }
        loadAllRule(id);
        loadForest();
    }

    public HashMap<String, Object> parse(JSONObject params, String log) {
        Integer id = Integer.valueOf(String.valueOf(params.get("id")));
        Node node = nodeMap.get(id);
        while (node.hasParent()) {
            node = node.getParent();
        }
        Integer logTypeId = node.getLogTypeId();
        String tag = node.getTag();
        return parse(logTypeId, tag, log);
    }

    public int getNodeMapSize() {
        return nodeMap.size();
    }

    public int getTreeMapSize() {
        return treeMap.size();
    }
}
