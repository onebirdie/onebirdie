package haizhi.log.product.utils.ip;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

public class IPOperator {

    private static int offset;
    private static int[] index = new int[256];
    private static ByteBuffer dataBuffer;
    private static ByteBuffer indexBuffer;
    private static String path;

    public IPOperator() {
        path = "ip.dat";
//        try {
            load();
//        } catch (Exception e) {
//            System.out.println("load ip fail " + e.getMessage());
//            ;
//        }
    }

    public String parse(String ip) {
        try {
            String[] result = find(ip);
            return result[result.length - 1];
        } catch (Exception e) {
//            e.printStackTrace();
//            System.out.println("ip operator parse " + ip + " error " + e.getMessage());
            return null;
        }
    }

    private static String[] find(String ip) {
        int ip_prefix_value = new Integer(ip.substring(0, ip.indexOf(".")));
        long ip2long_value  = ip2long(ip);
        int start = index[ip_prefix_value];
        int max_comp_len = offset - 1028;
        long index_offset = -1;
        int index_length = -1;
        byte b = 0;
        for (start = start * 8 + 1024; start < max_comp_len; start += 8) {
            if (int2long(indexBuffer.getInt(start)) >= ip2long_value) {
                index_offset = bytesToLong(b, indexBuffer.get(start + 6), indexBuffer.get(start + 5), indexBuffer.get(start + 4));
                index_length = 0xFF & indexBuffer.get(start + 7);
                break;
            }
        }

        byte[] areaBytes;
        dataBuffer.position(offset + (int) index_offset - 1024);
        areaBytes = new byte[index_length];
        dataBuffer.get(areaBytes, 0, index_length);

        return new String(areaBytes, Charset.forName("UTF-8")).split("\t", -1);
    }

    private void load() {
//        lastModifyTime = ipFile.lastModified();
        InputStream fin = null;
        try {
            try {
                // 适用于执行代码文件时使用
                File f = new File(getClass().getResource("/" + path).getFile());
                dataBuffer = ByteBuffer.allocate(Long.valueOf(f.length()).intValue());
                fin = new FileInputStream(f);
                System.out.println("file method");
            } catch (Exception e) {
                // 适用于执行 jar 包时使用
                dataBuffer = ByteBuffer.allocate(1000 * 1000 * 15);
                fin = this.getClass().getResourceAsStream("/" + path);
                System.out.println("stream method");
//                e.printStackTrace();
            }
            int readBytesLength;
            byte[] chunk = new byte[65536];
            while (fin.available() > 0) {
                readBytesLength = fin.read(chunk);
//                System.out.println("readBytesLength " + readBytesLength);
                dataBuffer.put(chunk, 0, readBytesLength);
            }
            dataBuffer.position(0);
            int indexLength = dataBuffer.getInt();
            System.out.println("indexLength " + indexLength);
            byte[] indexBytes = new byte[indexLength];
            dataBuffer.get(indexBytes, 0, indexLength - 4);
            indexBuffer = ByteBuffer.wrap(indexBytes);
            indexBuffer.order(ByteOrder.LITTLE_ENDIAN);
            offset = indexLength;

            int loop = 0;
            while (loop++ < 256) {
                index[loop - 1] = indexBuffer.getInt();
            }
            indexBuffer.order(ByteOrder.BIG_ENDIAN);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {
            try {
                if (fin != null) {
                    fin.close();
                }
            } catch (IOException e){
                e.printStackTrace();
//                throw new Exception(e);
            }
        }
    }

    private static long bytesToLong(byte a, byte b, byte c, byte d) {
        return int2long((((a & 0xff) << 24) | ((b & 0xff) << 16) | ((c & 0xff) << 8) | (d & 0xff)));
    }

    private static int str2Ip(String ip)  {
        String[] ss = ip.split("\\.");
        int a, b, c, d;
        a = Integer.parseInt(ss[0]);
        b = Integer.parseInt(ss[1]);
        c = Integer.parseInt(ss[2]);
        d = Integer.parseInt(ss[3]);
        return (a << 24) | (b << 16) | (c << 8) | d;
    }

    private static long ip2long(String ip)  {
        return int2long(str2Ip(ip));
    }

    private static long int2long(int i) {
        long l = i & 0x7fffffffL;
        if (i < 0) {
            l |= 0x080000000L;
        }
        return l;
    }
}
