package haizhi.log.product.utils.parse;

import haizhi.log.product.utils.json.MyJson;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ysy on 17/6/26 14:59.
 */

public class GrokParser extends Parser {
    private Pattern pattern;
    private HashMap<String, String> fieldNameConvertMap;
    private JSONObject transform;

    public GrokParser(JSONObject rule) {
        super(rule);
        loadRule(rule);
    }

    public GrokParser (Integer id, Integer boundId, String tag, Integer logTypeId, JSONObject rule) {
        super(id, boundId, tag, logTypeId, rule);
        loadRule(rule);
    }

    public GrokParser (Integer id, Integer boundId, String tag, Integer logTypeId, JSONObject rule, String regex, String transform) {
        super(id, boundId, tag, logTypeId, rule);
        loadRule(rule);
    }

    public void loadRule(JSONObject rule) {
        haizhi.log.product.utils.json.MyJson myJson = new haizhi.log.product.utils.json.MyJson();
        this.transform = myJson.safeLoads(String.valueOf(rule.get("transform")));
        String originReg = String.valueOf(rule.get("regex"));
        load(originReg);
    }

    private void load(String originReg) {
        fieldNameConvertMap = new HashMap<>();

        // 将 (?P<>) 转换为 (?<>)
        originReg = originReg.replaceAll("\\(\\?P<", "(?<");

        // 提取所有字段
        String extractFieldReg = "(?<=\\(\\?<)\\w+";
        Pattern extractFieldPattern = Pattern.compile(extractFieldReg);
        Matcher extractFieldMatcher = extractFieldPattern.matcher(originReg);
        ArrayList<String> fields = new ArrayList<>();
        while (extractFieldMatcher.find()) {
            fields.add(extractFieldMatcher.group());
        }
//        System.out.println("fields " + fields);

        // 记录转换后的字段名与原始字段名的映射, 将转换后的字段名更新至解析规则
        String convertedName;
        for (String field : fields) {
            convertedName = field.replaceAll("[^a-zA-Z0-9]", "");
//            System.out.println(convertedName + " " + fields.get(i));
            fieldNameConvertMap.put(convertedName, field);
            originReg = originReg.replaceAll("<" + field + ">", "<" + convertedName + ">");
        }

        pattern = Pattern.compile(originReg);
    }

    public HashMap<String, Object> parse(String log) {
        HashMap<String, Object> result = new HashMap<>();

        Matcher matcher = pattern.matcher(log);
//        if (!matcher.matches()) {
//            System.out.println("mismatch");
//            return null;
//        }
        while (matcher.find()) {
            for (Map.Entry<String, String> entry : fieldNameConvertMap.entrySet()) {
                try {
                    String value = matcher.group(entry.getKey());
                    if (! "".equals(value)) {
                        result.put(entry.getValue(), value);
                    }
                } catch (Exception e0) {
                }
            }
        }

        if (result.size() == 0)
            return null;

        String key;
        for (Map.Entry<String, Object> entry : result.entrySet()) {
            key = entry.getKey();
            if (transform.containsKey(key)) {
                if (transform.get(key).equals("int")) {
                    try {
                        Integer value = Integer.parseInt(String.valueOf(entry.getValue()));
                        result.put(key, value);
                    } catch(Exception e1) {
                    }
                }
                if (transform.get(key).equals("float")) {
                    try {
                        Float value = Float.parseFloat(String.valueOf(entry.getValue()));
                        result.put(key, value);
                    } catch (Exception e2) {
                    }
                }
            }
        }

        return result;
    }
}
