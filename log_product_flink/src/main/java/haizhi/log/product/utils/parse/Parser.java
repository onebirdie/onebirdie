package haizhi.log.product.utils.parse;

import org.json.simple.JSONObject;

import java.util.HashMap;

/**
 * Created by ysy on 17/7/12 16:25.
 */

public abstract class Parser {
    private Integer id;
    private Integer boundId;
    private String tag;
    private Integer logTypeId;
    private JSONObject rule;

    public Parser(JSONObject rule) {
    }

    public Parser(Integer id, Integer boundId, String tag, Integer logTypeId, JSONObject rule) {
        this.id = id;
        this.boundId = boundId;
        this.tag = tag;
        this.logTypeId = logTypeId;
        this.rule = rule;
    }

    abstract HashMap<String, Object> parse(String log);

    public Integer getId() {
        return id;
    }

    public Integer getLogTypeId() {
        return logTypeId;
    }

    public String getTag() {
        return tag;
    }

    public Integer getBoundId() {
        return boundId;
    }
}
