package haizhi.log.product.utils.parse;

/**
 * Created by ysy on 18/6/1 16:58.
 */

public class ParseNodeStats {

    private long times;
    private long totalTimeConsumingMs;

    public ParseNodeStats() {
        this.times = 0L;
        this.totalTimeConsumingMs = 0L;
    }

    public long getTimes() {
        return times;
    }

    public long getTotalTimeConsumingMs() {
        return totalTimeConsumingMs;
    }

    public void update(long consumingMs) {
        totalTimeConsumingMs += consumingMs;
        times += 1L;
    }

    public void clear() {
        this.times = 0L;
        this.totalTimeConsumingMs = 0L;
    }
}
