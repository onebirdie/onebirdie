package haizhi.log.product.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * Created by ysy on 17/6/26 11:57.
 */

public class MyJson {
    private JSONParser parser;

    public MyJson() {
        parser = new JSONParser();
    }

    public JSONObject safeLoads(String jsonStr) {
        try {
            return (JSONObject) parser.parse(jsonStr);
        } catch (Exception e0) {
            return null;
        }
    }

    public JSONObject loads(String jsonStr) throws Exception {
        return (JSONObject) parser.parse(jsonStr);
    }

    public JSONArray safeLoadsArray(String jsonStr) {
        try {
            return (JSONArray) parser.parse(jsonStr);
        } catch (Exception e0) {
            return null;
        }
    }

    public String dumps(JSONObject jsonObj) {
        return jsonObj.toJSONString();
    }

}

