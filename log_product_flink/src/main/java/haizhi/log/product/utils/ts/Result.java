package haizhi.log.product.utils.ts;

import java.time.LocalDateTime;

/**
 * Created by ysy on 17/7/5 13:21.
 */

class Result {

    Integer year = null;
    Integer month = null;
    Integer day = null;
    Integer weekday = null;
    Integer hour = null;
    Integer minute = null;
    Integer second = null;
    Integer microsecond = null;
    Boolean century_specified = null;

    Result() {

    }

    Integer size() {
        Integer size = 0;
        if (year != null)
            size += 1;
        if (month != null)
            size += 1;
        if (day != null)
            size += 1;
        if (weekday != null)
            size += 1;
        if (hour != null)
            size += 1;
        if (minute != null)
            size += 1;
        if (second != null)
            size += 1;
        if (microsecond != null)
            size += 1;
        if (century_specified != null)
            size += 1;

        return size;
    }

    LocalDateTime replace(LocalDateTime defaultTime) {
        if (year != null)
            defaultTime = defaultTime.withYear(year);
        if (month != null)
            defaultTime = defaultTime.withMonth(month);
        if (day != null)
            defaultTime = defaultTime.withDayOfMonth(day);
        if (hour != null)
            defaultTime = defaultTime.withHour(hour);
        if (minute != null)
            defaultTime = defaultTime.withMinute(minute);
        if (second != null)
            defaultTime = defaultTime.withSecond(second);
        return defaultTime;
    }

    @Override
    public String toString() {
        return "result " + year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second + ',' + microsecond;
    }
}

