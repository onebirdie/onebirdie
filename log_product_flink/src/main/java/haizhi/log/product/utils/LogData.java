package haizhi.log.product.utils;

import haizhi.log.product.utils.json.MyJson;
import org.apache.logging.log4j.LogManager;
import org.json.simple.JSONObject;

import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.HashMap;

/**
 * Created by ysy on 17/10/30 16:30.
 */

public class LogData implements Serializable {


    private static final long serialVersionUID = 1L;
    private static org.apache.logging.log4j.Logger log = LogManager.getLogger("saas");
    private String version = "v2.0";
    private String token;
    private String host;
    private String path = "";
    private String raw;
    private long ts = 0L;
    private long contextId = 0L;
    private String parsedFieldsJsonStr = "";

    public LogData(String str) {
        split(str);
    }

    public LogData(ByteBuffer byteBuffer) throws Exception {
        Charset charset;
        CharsetDecoder decoder;
        CharBuffer charBuffer;

        charset = Charset.forName("UTF-8");
        decoder = charset.newDecoder();
        charBuffer = decoder.decode(byteBuffer.asReadOnlyBuffer());
        String strBuffer = charBuffer.toString();
        if (strBuffer.startsWith("{")) {
            splitQiNiu(strBuffer);
        } else{
            split(strBuffer);
        }
    }

    private void splitQiNiu(String str) {
        str = str.trim();
        haizhi.log.product.utils.json.MyJson myJson = new haizhi.log.product.utils.json.MyJson();
        JSONObject jsonObject = myJson.safeLoads(str);
        if (jsonObject == null) {
            log.error("parse qiniu data error " + str);
            throw new RuntimeException("parse qiniu data error " + str);
        } else {
            if (jsonObject.containsKey("raw")) {
                setRaw(String.valueOf(jsonObject.get("raw")));
            } else {
                log.error("qiniu data do not contain raw in " +  str);
                throw new RuntimeException("qiniu data do not contain raw in " + str);
            }

            if (jsonObject.containsKey("datasource")) {
                setHost(String.valueOf(jsonObject.get("datasource")));
            } else {
                log.error("qiniu data do not contain datasource in " +  str);
                throw new RuntimeException("qiniu data do not contain datasource in " + str);
            }

            if (jsonObject.containsKey("token")) {
                setToken(String.valueOf(jsonObject.get("token")));
            } else {
                log.error("qiniu data do not contain token in " +  str);
                throw new RuntimeException("qiniu data do not contain token in " + str);
            }

            if (jsonObject.containsKey("hostname")) {
                setHost(String.valueOf(jsonObject.get("hostname")));
            } else {
                log.error("qiniu data do not contain hostname in " +  str);
                throw new RuntimeException("qiniu data do not contain hostname in " + str);
            }
        }
    }

    private void split(String string) {
        String[] fields = string.split("\n", 4);
//        System.out.println(Arrays.asList(fields));
        if (fields.length < 3) {
            throw new RuntimeException("invalid line number: " + fields.length + " " + string);
        }
        put(fields);
    }

    private void put(String[] fields) {
        if (fields.length >= 4) {
            this.version = "v2.0";
            setToken(fields[0]);
            setHost(fields[1]);
            setPath(fields[2]);

            // 兼容某些 udp 渠道日志自带换行符（河北银行）
            StringBuilder sb = new StringBuilder(4);
            for (int i = 3; i < fields.length; i++) {
                sb.append(fields[i]);
            }
            setRaw(sb.toString());
        } else if (fields.length == 3) {
            this.version = "v1.0";
            setToken(fields[0]);
            setHost(fields[1]);
            setRaw(fields[2]);
        }
    }

    public String getToken() {
        return this.token;
    }

    private void setToken(String token) {
        this.token = token;
    }

    public String getHost() {
        return this.host;
    }

    private void setHost(String host) {
        this.host = host;
    }

//    public String getPath() {
//        return this.path;
//    }

    private void setPath(String path) {
        this.path = path.trim();
    }

    public String getRaw() {
        return this.raw;
    }

    private void setRaw(String raw) {
        this.raw = raw;
    }

    public long getTs() {
        return this.ts;
    }

    public void setTs(Long ts) {
        if (ts == null || ts < 0) {
            this.ts = 0L;
        } else {
            this.ts = ts;
        }
    }

//    public long getContextId() {
//        return this.contextId;
//    }

    public void setContextId(long contextId) {
        this.contextId = contextId;
    }

    public void setParsedFieldsJsonStr(String parsedFieldsJsonStr) {
        this.parsedFieldsJsonStr = parsedFieldsJsonStr;
    }

//    public String getParsedFieldsJsonStr() {
//        return this.parsedFieldsJsonStr;
//    }

    public String getUniqueId() {
        return this.token + " " + this.host;
    }

    public void appendRaw(LogData logData) {
        this.raw += "\n" + logData.getRaw();
    }

    public String toString() {
        return token + "\n" + host + "\n" + path + "\n" + String.valueOf(ts) + "\n" + raw + "\n" + parsedFieldsJsonStr;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> jsonLog = new HashMap<>();
        jsonLog.put("_token", this.token);
        jsonLog.put("_host", this.host);
        jsonLog.put("_raw", this.raw);
        jsonLog.put("_context_id", this.contextId);
        jsonLog.put("rts", System.currentTimeMillis());
        if (this.ts == 0) {
            this.ts = System.currentTimeMillis();
        }
        jsonLog.put("_ts", this.ts);
        if ("v2.0".equals(this.version) && ! "".equals(this.path)) {
            jsonLog.put("_path", this.path);
        }

        if (! "".equals(this.parsedFieldsJsonStr)) {
            haizhi.log.product.utils.json.MyJson myJson = new haizhi.log.product.utils.json.MyJson();
            try {
                jsonLog.putAll(myJson.loads((this.parsedFieldsJsonStr)));
            } catch (Exception e0) {
//                e0.printStackTrace();
//                System.out.println("load parsed fields Json str fail " + this.parsedFieldsJsonStr + " " + e0.getMessage());
            log.error("load parsed fields Json str fail " + this.parsedFieldsJsonStr + " " + e0.getMessage());
            }
        }
        return jsonLog;
    }

    public int getProximateSize() {
        return this.parsedFieldsJsonStr.length() + this.raw.length() + 80;
    }

}
