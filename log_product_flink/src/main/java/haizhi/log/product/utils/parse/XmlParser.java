package haizhi.log.product.utils.parse;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

//import java.util.ArrayList;

/**
 * Created by ysy on 17/7/12 12:08.
 */

public class XmlParser extends Parser{
    private int begin;
    private int end;
    private boolean nested;

    public static class Tuple {
        private Element element;
        private String prefix;

        public Tuple(Element element, String prefix) {
            this.element = element;
            this.prefix = prefix;
        }

        public Element getElement() {
            return element;
        }
        public String getPrefix() {
            return prefix;
        }

    }

    public XmlParser(JSONObject rule) {
        super(rule);
        loadRule(rule);
    }

    public XmlParser(Integer id, Integer boundId, String tag, Integer logTypeId, JSONObject rule) {
        super(id, boundId, tag, logTypeId, rule);
        loadRule(rule);
    }

    private void loadRule(JSONObject rule) {
        this.begin = Integer.parseInt(String.valueOf(rule.get("xml_begin_line")));
        if (rule.containsKey("xml_end_line"))
            this.end = Integer.parseInt(String.valueOf(rule.get("xml_end_line")));
        else
            this.end = 1;
        this.nested = Boolean.valueOf(String.valueOf(rule.get("xml_nested")));
    }

    public HashMap<String, Object> parse(String log) {

        String[] lines = log.split("\n");
        int realEnd = lines.length + 1 - end;
        if (realEnd < begin)
            return null;
//        if (begin == 1) {
//            begin = realEnd;
//        }
        String[] logs = Arrays.copyOfRange(lines, begin - 1, realEnd);
        log = String.join("\n", logs);
//        System.out.println("xml parser parse " + log);
        if (log.startsWith("responseInfo")) {
            log = log.replaceAll("responseInfo\\s*:\\s*", "");
        }
//        System.out.println("real log " + log);

        HashMap<String, Object> result = new HashMap<>();
        ArrayList<Tuple> stack = new ArrayList<>();
//        SAXReader reader = new SAXReader();
        try {
//            Document document = reader.read(log);
            Document document = DocumentHelper.parseText(log);
            Element root = document.getRootElement();
            stack.add(new Tuple(root, ""));
            while (! stack.isEmpty()) {
                ArrayList<Tuple> children = new ArrayList<>();
                for (Tuple tuple : stack) {
                    Element element = tuple.getElement();
                    String prefix = tuple.getPrefix();
                    input(result, element.getName(), element.getTextTrim(), prefix);
                    List<Element> elementChildren = element.elements();
                    for (Element child : elementChildren) {
                        children.add(new Tuple(child, prefix + "_" + element.getName()));
                    }
                }
                stack = children;
            }

            return result;
        } catch (Exception e1) {
//            System.out.println("parse xml error " + e1.getMessage() + "log " + log);
            return null;
//            throw new RuntimeException(e1);
        }
    }

    private void input(HashMap<String, Object> result, String key, String value, String prefix) {
        if (value == null || "".equals(value))
            return;
        if (nested) {
            if (!(prefix == null || "".equals(prefix))) {
                prefix += '_';
            }
            result.put(prefix + key, value);
        }
        else
            result.put(key, value);
    }
}
