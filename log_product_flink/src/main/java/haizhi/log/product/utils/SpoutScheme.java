package haizhi.log.product.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.storm.spout.Scheme;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Values;
import org.nustaq.serialization.FSTConfiguration;

import java.nio.ByteBuffer;
import java.util.List;

/**
 * Created by ysy on 17/6/27 22:30.
 */

public class SpoutScheme implements Scheme {
    static final FSTConfiguration conf = FSTConfiguration.createDefaultConfiguration();
    private static Logger log = LogManager.getLogger("saas");

    public List<Object> deserialize(ByteBuffer byteBuffer) {
        try {
            LogData logData = new LogData(byteBuffer);
            String token = logData.getToken();
            String host = logData.getHost();

            Values values = new Values();
            values.add(token);
            values.add(host);
            byte[] logDataByte = conf.asByteArray(logData);
//            byte[] logDataByte = SerializationUtils.serialize(logData);
            values.add(logDataByte);
            return values;
        } catch (Exception e) {
            log.warn("spout parse fail " + e.getMessage() + " byte " + byteBuffer.toString());
            log.error("spout parse fail stack trace: ", e);
//            e.printStackTrace();
            return null;
        }
    }

    public Fields getOutputFields() {
        return new Fields("token", "host", "log");
    }
}
