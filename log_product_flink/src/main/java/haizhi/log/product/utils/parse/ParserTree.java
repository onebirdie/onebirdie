package haizhi.log.product.utils.parse;

import java.util.HashMap;

/**
 * Created by ysy on 17/7/17 00:03.
 */

public class ParserTree {
    private Node root;

    public ParserTree(Node root) {
        this.root = root;
    }

    public HashMap<String, Object> parse(String log) {
        HashMap<String, Object> result = new HashMap<>();
        return root.parse(log, result);
    }

    public Node getRoot() {
        return root;
    }
}
