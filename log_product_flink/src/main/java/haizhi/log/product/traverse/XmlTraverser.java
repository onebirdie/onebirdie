package haizhi.log.product.traverse;

import org.dom4j.Element;

import java.util.List;

public class XmlTraverser extends BaseTraverser<Element, String, String, Object> {

    public XmlTraverser(Element treeObj) {
        super(treeObj);
    }

    public XmlTraverser(Element root, boolean nested) {
        super(root, nested);
    }

    @Override
    Iterable<Element> getChildren(Element elem) {
        List<Element> elements = elem.elements();
        return elements.size() > 0 ? elements : null;
    }

    @Override
    boolean isLeaf(Element elem) {
        // TODO: 暂时只处理文本节点
        return elem.isTextOnly();
    }

    @Override
    String getKey(Element elem) {
        return elem.getName();
    }

    @Override
    String getVal(Element elem) {
        return elem.getTextTrim();
    }

    @Override
    String getInitPrefix() {
        return "";
    }

    @Override
    String getPrefixKey(String keyPrefix, String key) {
        if (!keyPrefix.isEmpty()) {
            return keyPrefix + "_" + key;
        }

        return key;
    }

    @Override
    Object getResultVal(String value) {
        return value;
    }
}
