package haizhi.log.product.traverse;

import java.util.HashMap;

public abstract class BaseTraverser<T, K, V, R> {
    public T root;
    public boolean nested;

    public BaseTraverser(T root) {
        this(root, true);
    }

    public BaseTraverser(T root, boolean nested) {
        this.root = root;
        this.nested = nested;
    }

    abstract Iterable<T> getChildren(T elem);

    abstract boolean isLeaf(T elem);

    abstract K getKey(T elem);

    abstract V getVal(T elem);

    abstract K getInitPrefix();

    abstract K getPrefixKey(K keyPrefix, K key);

    abstract R getResultVal(V value);

    public HashMap<K, R> traverse() {
        HashMap<K, R> resultMap = new HashMap<>();
        K keyPrefix = getInitPrefix();

        resultMap = traverseTree(root, resultMap, keyPrefix);

        return resultMap;
    }

    private HashMap<K, R> traverseTree(T root, HashMap<K, R> resultMap, K keyPrefix) {
        K key = getKey(root);

        keyPrefix = nested ? getPrefixKey(keyPrefix, key) : key;

        if (isLeaf(root)) {
            R val = getResultVal(getVal(root));
            if (val != null) {
                resultMap.put(keyPrefix, val);
            }
        } else {
            Iterable<T> children = getChildren(root);
            if (children != null) {
                for (T child : children) {
                    traverseTree(child, resultMap, keyPrefix);
                }
            }
        }

        return resultMap;
    }
}
