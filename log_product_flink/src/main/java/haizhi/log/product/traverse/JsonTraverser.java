package haizhi.log.product.traverse;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;

import java.util.Map;


public class JsonTraverser extends BaseTraverser<Map.Entry<String, JsonElement>, String, JsonElement, Object> {

    public JsonTraverser(Map.Entry<String, JsonElement> root, boolean nested) {
        super(root, nested);
    }

    public JsonTraverser(Map.Entry<String, JsonElement> root) {
        super(root);
    }

    @Override
    Iterable<Map.Entry<String, JsonElement>> getChildren(Map.Entry<String, JsonElement> elem) {
        JsonElement value =  elem.getValue();
        if (value.isJsonObject()) {
            return value.getAsJsonObject().entrySet();
        } else if (value.isJsonArray()) {
            // TODO: 暂时不处理数组类型
            return null;
        }

        return null;
    }

    @Override
    boolean isLeaf(Map.Entry<String, JsonElement> elem) {
        return elem.getValue().isJsonPrimitive() || elem.getValue().isJsonNull();
    }

    @Override
    String getKey(Map.Entry<String, JsonElement> elem) {
        return elem.getKey();
    }

    @Override
    JsonElement getVal(Map.Entry<String, JsonElement> elem) {
        return elem.getValue();
    }

    @Override
    String getInitPrefix() {
        return "";
    }

    @Override
    String getPrefixKey(String keyPrefix, String key) {
        if (!keyPrefix.isEmpty()) {
            return keyPrefix + "_" + key;
        }

        return key;
    }

    @Override
    Object getResultVal(JsonElement value) {
        if (value.isJsonPrimitive()) {
            JsonPrimitive newValue =  value.getAsJsonPrimitive();
            if(newValue.isString())
                return newValue.getAsString();
            else if(newValue.isNumber())
                return newValue.getAsNumber();
            else if (newValue.isBoolean())
                return newValue.getAsBoolean();
        }

        return null;
    }
}
