package haizhi.log.product

import java.util.Properties

import org.apache.commons.configuration.{Configuration, PropertiesConfiguration}
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer
import org.apache.flink.streaming.util.serialization.SimpleStringSchema

object LogCollectFlink {
  def main(args: Array[String]): Unit = {

    //1.接收日志文件中的参数
    val config: PropertiesConfiguration = new PropertiesConfiguration(args(0))

    //2.获取env对象
    val env = StreamExecutionEnvironment.getExecutionEnvironment


    //3.创建kafka Source 接收资源
    val props = new Properties()
    props.setProperty("zookeeper", "JD1:2181,JD2:2181,JD3:2181")

    //
    new FlinkKafkaConsumer[String](config.getString("kafka.read.topic"), new
        SimpleStringSchema(), props)
    //3.创建DataStream
//    val value = env.addSource()



  }
}

