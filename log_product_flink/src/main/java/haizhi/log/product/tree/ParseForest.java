package haizhi.log.product.tree;

import haizhi.log.product.utils.parse.ParseNodeStats;

import java.util.HashMap;
import java.util.Map;

public class ParseForest{
    private HashMap<Integer, ParseNode> parseNodeMap;
    private HashMap<String, ParseNode> parseTreeMap = new HashMap<>();

    public ParseForest(HashMap<Integer, ParseNode> parseNodeMap) {
        this.parseNodeMap = parseNodeMap;

        initParseTreeMap();
    }

    private void initParseTreeMap() {
        for (Map.Entry<Integer, ParseNode> entry : parseNodeMap.entrySet()) {
            ParseNode node = entry.getValue();

            if(node == null){
                continue;
            }

            handleNode(node);
        }
    }

    public void clearStats() {
        for (Integer nodeId : parseNodeMap.keySet()) {
            parseNodeMap.get(nodeId).clearStats();
        }
    }

    public HashMap<Integer, ParseNodeStats> exportParseStats() {
        HashMap<Integer, ParseNodeStats> parseNodeStatsMap = new HashMap<>();
        for (Integer nodeId : parseNodeMap.keySet()) {
            parseNodeStatsMap.put(nodeId, parseNodeMap.get(nodeId).getParseStats());
        }
        return parseNodeStatsMap;
    }

    private void handleNode(ParseNode node) {
        Integer boundId = node.getBoundId();
        if (boundId != null && boundId != 0) {
            ParseNode parentNode = parseNodeMap.get(boundId);

            if (parentNode != null) {
                node.setParent(parentNode);
                parentNode.addChild(node);
            }
        } else {
            putNode(node);
        }
    }

    private String genKey(Integer logTypeId, String tag){
        if (tag == null || "".equals(tag))
            return null;
        else
            return String.valueOf(logTypeId) + "_" + tag;
    }

    private String genKey(ParseNode node) {
        return genKey(node.getLogTypeId(), node.getTag());
    }

    private void putNode(ParseNode node) {
        String key = genKey(node);
        if (key != null && ! parseTreeMap.containsKey(key)){
            parseTreeMap.put(key, node);
        }
    }

    private ParseNode getNode(Integer logTypeId, String tag) {
        String key = genKey(logTypeId, tag);
        if (key != null && ! parseTreeMap.containsKey(key)){
            return null;
        } else {
            return parseTreeMap.get(key);
        }
    }

    public HashMap<String, Object> parse(ParseNode node, String log) {
        if (node == null) {
            return null;
        } else {
            return node.parse(log);
        }
    }

    public HashMap<String, Object> parse(Integer logTypeId, String tag, String log) {
        ParseNode node = getNode(logTypeId, tag);

        return parse(node, log);
    }

    public HashMap<String, Object> testParse(ParseNode node, String log) {
        parseNodeMap.put(node.getId(), node);

        handleNode(node);

        ParseNode root = getRootNode(node.getId());

        return parse(root, log);
    }

    private ParseNode getRootNode(Integer key) {
        ParseNode node = parseNodeMap.get(key);
        while (node != null && node.getParent() != null) {
            node = node.getParent();
        }

        return node;
    }
}
