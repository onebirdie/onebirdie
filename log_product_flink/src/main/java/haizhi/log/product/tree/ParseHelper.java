package haizhi.log.product.tree;

import haizhi.log.product.utils.json.MyJson;
import haizhi.log.product.utils.mysql.MysqlUtils;
import haizhi.log.product.utils.parse.ParseNodeStats;
import org.json.simple.JSONObject;

import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

public class ParseHelper {
    private MysqlUtils mysql;
    private ParseForest parseForest;

    public ParseHelper(String domain, Map conf) {
        try {
            mysql = new MysqlUtils(domain, conf);
        } catch (Exception e0) {
            throw new RuntimeException(e0);
        }

        updateParser();
    }

    public void updateParser() {
        parseForest = new ParseForest(getParseNodeMap(null));
    }

    private HashMap<Integer, ParseNode> getParseNodeMap(Integer ignoreId) {
        String sql = "select id, bound_id, tag, type, log_type_id, rule " +
                "from log_parse_rule " +
                "where disabled=0 and is_deleted=0";
        if (ignoreId != null)
            sql += " and id != " + String.valueOf(ignoreId);

        HashMap<Integer, ParseNode> parseNodeMap = new HashMap<>();
        MyJson myJson = new MyJson();
        try {
            ResultSet results = mysql.executeQuery(sql);
            while (results.next()) {
                Integer id = results.getInt("id");
                Integer boundId = results.getInt("bound_id");
                String tag = results.getString("tag");
                String type = results.getString("type");
                Integer logTypeId = results.getInt("log_type_id");
                String ruleStr = results.getString("rule");
                JSONObject rule = myJson.safeLoads(ruleStr);

                ParseNode node = new ParseNode(id, boundId, logTypeId, tag, type, rule);

                parseNodeMap.put(id, node);
            }
        } catch (Exception e1) {
            throw new RuntimeException(e1);
        }

        System.out.println("load " + String.valueOf(parseNodeMap.size()) + " rules");

        return parseNodeMap;
    }

    public void clearStats() {
        parseForest.clearStats();
    }

    public HashMap<Integer, ParseNodeStats> exportParseStats() {
        return parseForest.exportParseStats();
    }

    public HashMap<String, Object> parse(Integer logTypeId, String tag, String log) {
        return parseForest.parse(logTypeId, tag, log);
    }

    public HashMap<String, Object> testAllParse(JSONObject params, String log) {
        ParseNode parseNode = genParseNode(params);

        return parseForest.testParse(parseNode, log);
    }

    private ParseNode genParseNode(JSONObject params) {
        Integer id;
        if (! params.containsKey("id") || params.get("id") == null) {  // 新规则
            id = -1;
        } else {
            id = Integer.valueOf(String.valueOf(params.get("id")));
        }

        Integer boundId;
        if (params.containsKey("bound_id") && params.get("bound_id") != null
                && ! "".equals(String.valueOf(params.get("bound_id"))))
            boundId = Integer.valueOf(String.valueOf(params.get("bound_id")));
        else
            boundId = null;

        Integer logTypeId = Integer.valueOf(String.valueOf(params.get("log_type_id")));

        String tag = String.valueOf(params.get("tag"));
        String type = String.valueOf(params.get("type"));
        JSONObject rule = (JSONObject) params.get("rule");

        return new ParseNode(id, boundId, logTypeId, tag, type, rule);
    }
}
