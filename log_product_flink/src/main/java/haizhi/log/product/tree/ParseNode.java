package haizhi.log.product.tree;

import haizhi.log.product.params.BaseParams;
import haizhi.log.product.parser.BaseParser;
import haizhi.log.product.parser.ParseFactory;
import haizhi.log.product.utils.parse.ParseNodeStats;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ParseNode {
    private Integer id;
    private Integer boundId;
    private Integer logTypeId;
    private String tag;
    private String parseType;
    private JSONObject parseRule;

    private BaseParser<? extends BaseParams> parser;

    private ParseNode parent;
    private ArrayList<ParseNode> children = new ArrayList<>();

    private ParseNodeStats parseStats;

    public ParseNode(Integer id, Integer boundId, Integer logTypeId, String tag, String parseType, JSONObject parseRule) {
        this.id = id;
        this.boundId = boundId;
        this.logTypeId = logTypeId;
        this.tag = tag;
        this.parseType = parseType;
        this.parseRule = parseRule;

        parser = ParseFactory.getParser(parseType, parseRule);
        this.parseStats = new ParseNodeStats();
    }

    public void clearStats() {
        this.parseStats.clear();
    }

    public Integer getId() {
        return id;
    }

    public Integer getBoundId() {
        return boundId;
    }

    public String getTag() {
        return tag;
    }

    public Integer getLogTypeId() {
        return logTypeId;
    }

    public String getParseType() {
        return parseType;
    }

    public JSONObject getParseRule() {
        return parseRule;
    }

    public BaseParser getParser() {
        return parser;
    }

    public void setParent(ParseNode parent){
        this.parent = parent;
    }

    public void addChild(ParseNode child) {
        children.add(child);
    }

    public ParseNode getParent() {
        return parent;
    }

    public ArrayList<ParseNode> getChildren() {
        return children;
    }

    public ParseNodeStats getParseStats() {
        return parseStats;
    }

    public HashMap<String, Object> parse(String text) {
        HashMap<String, Object> result = null;

        long beginMs = System.currentTimeMillis();
        try {
            result = ParseFactory.getParsedResult(parser, text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        long consumingMs = System.currentTimeMillis() - beginMs;
        updateParseStats(consumingMs);


        if (result != null) {
            for (ParseNode child : children) {
                HashMap<String, Object> childResult = child.parse(text);
                if (childResult != null && childResult.size() > 0) {
                    result.putAll(childResult);

                    return result;
                }
            }
        }

        return result;
    }

    public void updateParseStats(long use) {
        this.parseStats.update(use);
    }
}
